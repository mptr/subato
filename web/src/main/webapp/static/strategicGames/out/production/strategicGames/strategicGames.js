if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'strategicGames'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'strategicGames'.");
}
var strategicGames = function (_, Kotlin) {
  'use strict';
  var throwCCE = Kotlin.throwCCE;
  var math = Kotlin.kotlin.math;
  var ensureNotNull = Kotlin.ensureNotNull;
  var Unit = Kotlin.kotlin.Unit;
  var Kind_CLASS = Kotlin.Kind.CLASS;
  var toByte = Kotlin.toByte;
  var appendText = Kotlin.kotlin.dom.appendText_46n0ku$;
  var Kind_INTERFACE = Kotlin.Kind.INTERFACE;
  var get_js = Kotlin.kotlin.js.get_js_1yb8b7$;
  var equals = Kotlin.equals;
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var toString = Kotlin.toString;
  var Pair = Kotlin.kotlin.Pair;
  var emptyList = Kotlin.kotlin.collections.emptyList_287e2$;
  var Kind_OBJECT = Kotlin.Kind.OBJECT;
  var kotlin_js_internal_IntCompanionObject = Kotlin.kotlin.js.internal.IntCompanionObject;
  Muehle.prototype = Object.create(AbstractRegularGame.prototype);
  Muehle.prototype.constructor = Muehle;
  NoMove.prototype = Object.create(OthelloMove.prototype);
  NoMove.prototype.constructor = NoMove;
  Othello.prototype = Object.create(AbstractRegularGame.prototype);
  Othello.prototype.constructor = Othello;
  TicTacToe.prototype = Object.create(AbstractRegularGame.prototype);
  TicTacToe.prototype.constructor = TicTacToe;
  Vier.prototype = Object.create(AbstractRegularGame.prototype);
  Vier.prototype.constructor = Vier;
  function negaMax(g, tiefe, alpha, beta) {
    var tmp$;
    var a = alpha;
    if (tiefe === 0 || g.ended())
      return g.evalState_s8j3t7$(g.currentPlayer());
    tmp$ = g.interestingMoves().iterator();
    while (tmp$.hasNext()) {
      var i = tmp$.next();
      var wert = -negaMax(g.doMove_11rb$(i), tiefe - 1 | 0, -beta | 0, -a | 0) | 0;
      if (wert >= beta)
        return beta;
      if (wert > a)
        a = wert;
    }
    return a;
  }
  function bestMove(g, depth) {
    var tmp$;
    var val = -2147483647;
    var result = null;
    tmp$ = g.interestingMoves().iterator();
    while (tmp$.hasNext()) {
      var m = tmp$.next();
      var s = g.doMove_11rb$(m);
      var eval_0 = -negaMax(s, depth, -2147483647, -val | 0) | 0;
      if (eval_0 > val) {
        val = eval_0;
        result = m;
      }
       else if (result == null)
        result = m;
    }
    return result;
  }
  var Array_0 = Array;
  function GameBoard(game) {
    this.game = game;
    this.g = this.game;
    this.div = document.createElement('div');
    var array = Array_0(this.game.columns);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      var array_0 = Array_0(this.game.rows);
      var tmp$_0;
      tmp$_0 = array_0.length - 1 | 0;
      for (var i_0 = 0; i_0 <= tmp$_0; i_0++) {
        array_0[i_0] = new GameBoard$Field(this, 0, 0);
      }
      array[i] = array_0;
    }
    this.fs = array;
    var tmp$_1, tmp$_2;
    this.g = this.game;
    tmp$_1 = this.game.rows;
    for (var r = 0; r < tmp$_1; r++) {
      tmp$_2 = this.game.columns;
      for (var c = 0; c < tmp$_2; c++) {
        var rr = this.game.rows - r - 1 | 0;
        this.fs[c][rr] = new GameBoard$Field(this, toByte(c), toByte(rr));
        this.div.appendChild(this.fs[c][rr].canvas);
      }
      this.div.appendChild(document.createElement('br'));
    }
  }
  GameBoard.prototype.paint = function () {
    var tmp$, tmp$_0, tmp$_1;
    tmp$ = this.fs;
    for (tmp$_0 = 0; tmp$_0 !== tmp$.length; ++tmp$_0) {
      var c = tmp$[tmp$_0];
      for (tmp$_1 = 0; tmp$_1 !== c.length; ++tmp$_1) {
        var f = c[tmp$_1];
        f.paint();
      }
    }
  };
  function GameBoard$Field($outer, c, r, canvas) {
    this.$outer = $outer;
    var tmp$;
    if (canvas === void 0)
      canvas = Kotlin.isType(tmp$ = document.createElement('canvas'), HTMLCanvasElement) ? tmp$ : throwCCE();
    this.c = c;
    this.r = r;
    this.canvas = canvas;
    this.canvas.height = 80;
    this.canvas.width = 80;
    this.canvas.addEventListener('click', GameBoard$GameBoard$Field_init$lambda(this.$outer, this));
    this.paint();
  }
  GameBoard$Field.prototype.paint = function () {
    var tmp$, tmp$_0, tmp$_1;
    var ctx = Kotlin.isType(tmp$ = this.canvas.getContext('2d'), CanvasRenderingContext2D) ? tmp$ : throwCCE();
    ctx.beginPath();
    ctx.arc(40.0, 40.0, 40.0, 0.0, 2 * math.PI);
    tmp$_0 = this.$outer.g.getAtPosition_5gdoe6$(this.c, this.r);
    if (tmp$_0 === this.$outer.g.playerNone)
      tmp$_1 = '#000000';
    else if (tmp$_0 === this.$outer.g.playerOne)
      tmp$_1 = '#FF0000';
    else
      tmp$_1 = '#0000FF';
    ctx.fillStyle = tmp$_1;
    ctx.fill();
    ctx.stroke();
  };
  function GameBoard$GameBoard$Field_init$lambda(this$GameBoard, this$Field) {
    return function (it) {
      var tmp$, tmp$_0;
      var m = ensureNotNull(this$GameBoard.g.createMove_5gdoe6$(this$Field.c, this$Field.r));
      if (this$GameBoard.g.currentPlayer() === this$GameBoard.g.playerOne && !this$GameBoard.g.ended() && this$GameBoard.g.legalMoves().contains_11rb$(m)) {
        this$GameBoard.g = Kotlin.isType(tmp$ = this$GameBoard.g.doMove_11rb$(m), RegularGame) ? tmp$ : throwCCE();
        this$GameBoard.paint();
        if (!this$GameBoard.g.ended()) {
          var bestMove_0 = ensureNotNull(bestMove(this$GameBoard.g, 6));
          this$GameBoard.g = Kotlin.isType(tmp$_0 = this$GameBoard.g.doMove_11rb$(bestMove_0), RegularGame) ? tmp$_0 : throwCCE();
          this$GameBoard.paint();
        }
      }
      return Unit;
    };
  }
  GameBoard$Field.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Field',
    interfaces: []
  };
  GameBoard.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'GameBoard',
    interfaces: []
  };
  function viergewinnt() {
    var tmp$, tmp$_0;
    var board = new GameBoard(new Vier());
    (tmp$ = document.getElementById('main')) != null ? tmp$.appendChild(board.div) : null;
    (tmp$_0 = document.getElementById('main')) != null ? appendText(tmp$_0, 'Spiele eine kleine Runde \xBBVier Gewinnt\xAB') : null;
  }
  function tictactoe() {
    var tmp$, tmp$_0;
    var board = new GameBoard(new TicTacToe());
    (tmp$ = document.getElementById('main')) != null ? tmp$.appendChild(board.div) : null;
    (tmp$_0 = document.getElementById('main')) != null ? appendText(tmp$_0, 'Spiele eine kleine Runde TicTacToe') : null;
  }
  function othello() {
    var tmp$, tmp$_0;
    var board = new GameBoard(new Othello());
    (tmp$ = document.getElementById('main')) != null ? tmp$.appendChild(board.div) : null;
    (tmp$_0 = document.getElementById('main')) != null ? appendText(tmp$_0, 'Spiele eine kleine Runde Othello') : null;
  }
  function Move() {
  }
  Move.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'Move',
    interfaces: []
  };
  function Position(triblet, pos) {
    this.triblet = triblet;
    this.pos = pos;
  }
  Position.prototype.toString = function () {
    return '(' + this.triblet + ',' + this.pos + ')';
  };
  var ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_ww73n8$;
  Position.prototype.connectedPositions = function () {
    var result = ArrayList_init();
    if (this.pos % 2 === 1) {
      if (this.triblet === 0 || this.triblet === 2)
        result.add_11rb$(new Position(toByte(1), this.pos));
      else {
        result.add_11rb$(new Position(toByte(0), this.pos));
        result.add_11rb$(new Position(toByte(2), this.pos));
      }
    }
    result.add_11rb$(new Position(this.triblet, toByte((this.pos + 7) % 8)));
    result.add_11rb$(new Position(this.triblet, toByte((this.pos + 1) % 8)));
    return result;
  };
  Position.prototype.rows = function () {
    var result = ArrayList_init();
    if (this.pos % 2 === 1) {
      var r1 = [new Position(toByte(0), this.pos), new Position(toByte(1), this.pos), new Position(toByte(2), this.pos)];
      var r2 = [new Position(this.triblet, toByte((this.pos + 7) % 8)), new Position(this.triblet, this.pos), new Position(this.triblet, toByte((this.pos + 1) % 8))];
      result.add_11rb$(r1);
      result.add_11rb$(r2);
    }
     else {
      var r1_0 = [new Position(this.triblet, toByte((this.pos + 1) % 8)), new Position(this.triblet, this.pos), new Position(this.triblet, toByte((this.pos + 2) % 8))];
      var r2_0 = [new Position(this.triblet, toByte((this.pos + 7) % 8)), new Position(this.triblet, this.pos), new Position(this.triblet, toByte((this.pos + 6) % 8))];
      result.add_11rb$(r1_0);
      result.add_11rb$(r2_0);
    }
    return result;
  };
  Position.prototype.equals = function (other) {
    var tmp$;
    if (this === other)
      return true;
    if (other == null || !equals(get_js(Kotlin.getKClassFromExpression(this)), get_js(Kotlin.getKClassFromExpression(other))))
      return false;
    Kotlin.isType(tmp$ = other, Position) ? tmp$ : throwCCE();
    if (this.triblet !== other.triblet)
      return false;
    if (this.pos !== other.pos)
      return false;
    return true;
  };
  Position.prototype.hashCode = function () {
    var result = this.triblet;
    result = (31 * result | 0) + this.pos;
    return result;
  };
  Position.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Position',
    interfaces: []
  };
  function SetStone() {
  }
  SetStone.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'SetStone',
    interfaces: [Move]
  };
  function MoveStone(from, position) {
    this.from = from;
    this.position_z5ib7t$_0 = position;
  }
  Object.defineProperty(MoveStone.prototype, 'position', {
    get: function () {
      return this.position_z5ib7t$_0;
    },
    set: function (position) {
      this.position_z5ib7t$_0 = position;
    }
  });
  MoveStone.prototype.move_wqgiqk$ = function (m) {
    var result = m.copy();
    result.playerStartStones[m.player % 2] = m.playerStartStones[m.player % 2];
    result.playerStartStones[m.nextPlayer() % 2] = m.playerStartStones[m.nextPlayer() % 2];
    result.b[this.position.triblet][this.position.pos] = m.player;
    result.b[this.from.triblet][this.from.pos] = m.playerNone;
    if (result.inMuehle_dfplqh$(this.position)) {
      result.removing = true;
      println('REMOVING WEGEN M\xDCHLE');
      return result;
    }
    result.movesDone = m.movesDone + 1 | 0;
    result.player = m.nextPlayer();
    return result;
  };
  MoveStone.prototype.toString = function () {
    return '[' + this.from + ' -> ' + this.position + ']';
  };
  MoveStone.prototype.equals = function (other) {
    var tmp$, tmp$_0, tmp$_1;
    if (this === other)
      return true;
    if (other == null || !equals(get_js(Kotlin.getKClassFromExpression(this)), get_js(Kotlin.getKClassFromExpression(other))))
      return false;
    Kotlin.isType(tmp$ = other, MoveStone) ? tmp$ : throwCCE();
    if (!((tmp$_0 = this.from) != null ? tmp$_0.equals(other.from) : null))
      return false;
    if (!((tmp$_1 = this.position) != null ? tmp$_1.equals(other.position) : null))
      return false;
    return true;
  };
  MoveStone.prototype.hashCode = function () {
    var result = this.from.hashCode();
    result = (31 * result | 0) + this.position.hashCode() | 0;
    return result;
  };
  MoveStone.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MoveStone',
    interfaces: [SetStone]
  };
  function Set() {
    this.position_8x3amd$_0 = null;
  }
  Object.defineProperty(Set.prototype, 'position', {
    get: function () {
      return this.position_8x3amd$_0;
    },
    set: function (position) {
      this.position_8x3amd$_0 = position;
    }
  });
  Set.prototype.move_wqgiqk$ = function (m) {
    var result = m.copy();
    result.playerStartStones[m.player % 2] = toByte(m.playerStartStones[m.player % 2] - 1);
    result.playerStartStones[m.nextPlayer() % 2] = m.playerStartStones[m.nextPlayer() % 2];
    result.b[this.position.triblet][this.position.pos] = m.player;
    if (result.inMuehle_dfplqh$(this.position)) {
      result.removing = true;
      return result;
    }
    result.movesDone = m.movesDone + 1 | 0;
    result.playerStartStones[m.player % 2] = toByte(m.playerStartStones[m.player % 2] - 1);
    result.playerStartStones[m.nextPlayer() % 2] = m.playerStartStones[m.nextPlayer() % 2];
    result.b[this.position.triblet][this.position.pos] = m.player;
    result.player = m.nextPlayer();
    return result;
  };
  Set.prototype.toString = function () {
    return '->' + toString(this.position);
  };
  Set.prototype.equals = function (other) {
    var tmp$;
    if (this === other)
      return true;
    if (other == null || !Kotlin.isType(other, Move))
      return false;
    if (Kotlin.isType(other, Remove)) {
      return this.equals(other.myMove);
    }
    if (!Kotlin.isType(other, SetStone))
      return false;
    if (!((tmp$ = this.position) != null ? tmp$.equals(other.position) : null))
      return false;
    return true;
  };
  Set.prototype.hashCode = function () {
    return this.position.hashCode();
  };
  Set.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Set',
    interfaces: [SetStone]
  };
  function Set_init(pos, $this) {
    $this = $this || Object.create(Set.prototype);
    Set.call($this);
    $this.position = pos;
    return $this;
  }
  function Set_init_0(tr, p, $this) {
    $this = $this || Object.create(Set.prototype);
    Set.call($this);
    $this.position = new Position(tr, p);
    return $this;
  }
  function Remove(myMove, pos) {
    this.myMove = myMove;
    this.rem = null;
    this.rem = RemoveStone_init(pos);
  }
  Remove.prototype.move_wqgiqk$ = function (m) {
    var result = this.myMove.move_wqgiqk$(m);
    this.rem.move_wqgiqk$(result);
    return result;
  };
  Remove.prototype.toString = function () {
    return '+' + this.myMove + '-' + this.rem;
  };
  Remove.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Remove',
    interfaces: [Move]
  };
  function RemoveStone() {
    this.position_pygj8c$_0 = null;
  }
  Object.defineProperty(RemoveStone.prototype, 'position', {
    get: function () {
      return this.position_pygj8c$_0;
    },
    set: function (position) {
      this.position_pygj8c$_0 = position;
    }
  });
  RemoveStone.prototype.move_wqgiqk$ = function (m) {
    var result = m.copy();
    m.b[this.position.triblet][this.position.pos] = m.playerNone;
    m.playerStones[m.player % 2] = toByte(m.playerStones[m.player % 2] - 1);
    result.removing = false;
    result.player = result.nextPlayer();
    return result;
  };
  RemoveStone.prototype.toString = function () {
    return '' + toString(this.position);
  };
  RemoveStone.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'RemoveStone',
    interfaces: [SetStone]
  };
  function RemoveStone_init(pos, $this) {
    $this = $this || Object.create(RemoveStone.prototype);
    RemoveStone.call($this);
    $this.position = pos;
    return $this;
  }
  function RemoveStone_init_0(tr, p, $this) {
    $this = $this || Object.create(RemoveStone.prototype);
    RemoveStone.call($this);
    $this.position = new Position(tr, p);
    return $this;
  }
  function Muehle() {
    AbstractRegularGame.call(this, 3, 8);
    this.playerStartStones = new Int8Array([9, 9]);
    this.playerStones = new Int8Array([9, 9]);
    this.marked = null;
    this.removing = false;
    this.prevState = null;
  }
  Muehle.prototype.createMove_5gdoe6$ = function (column, row) {
    println('createMove(' + column + ',' + row + ')');
    if (!this.removing) {
      println('!removing');
      println('movesDone: ' + this.movesDone);
      if (this.movesDone < 18)
        return Set_init(new Position(column, row));
      if (this.marked == null) {
        this.marked = new Position(column, row);
        return null;
      }
      var from = ensureNotNull(this.marked);
      this.marked = null;
      return new MoveStone(from, new Position(column, row));
    }
    return RemoveStone_init(new Position(column, row));
  };
  Muehle.prototype.getAtPosition_5gdoe6$ = function (column, row) {
    return this.b[column][row];
  };
  Muehle.prototype.atPos_dfplqh$ = function (p) {
    return this.b[p.triblet][p.pos];
  };
  Muehle.prototype.legalMoves = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4, tmp$_5;
    var result = ArrayList_init();
    var myStones = ArrayList_init();
    if (this.playerStartStones[this.player % 2] > 0) {
      tmp$ = this.columns;
      for (var trs = 0; trs < tmp$; trs++) {
        tmp$_0 = this.rows;
        for (var pos = 0; pos < tmp$_0; pos++) {
          if (this.b[trs][pos] === this.playerNone) {
            myStones.add_11rb$(Set_init_0(toByte(trs), toByte(pos)));
          }
        }
      }
    }
     else if (this.playerStones[this.player % 2] > 3) {
      tmp$_1 = this.columns;
      for (var trs_0 = 0; trs_0 < tmp$_1; trs_0++) {
        tmp$_2 = this.rows;
        for (var pos_0 = 0; pos_0 < tmp$_2; pos_0++) {
          if (this.b[trs_0][pos_0] === this.player) {
            var from = new Position(toByte(trs_0), toByte(pos_0));
            tmp$_3 = from.connectedPositions().iterator();
            while (tmp$_3.hasNext()) {
              var to = tmp$_3.next();
              if (this.b[to.triblet][to.pos] === AbstractRegularGame$Companion_getInstance().NONE) {
                myStones.add_11rb$(new MoveStone(from, to));
              }
            }
          }
        }
      }
    }
     else {
      var trs_1 = 0;
      while (trs_1 < this.columns) {
        var pos_1 = 0;
        while (pos_1 < this.rows) {
          var from_0 = new Position(trs_1, pos_1);
          if (this.player === this.atPos_dfplqh$(from_0)) {
            var trs2 = 0;
            while (trs_1 < this.columns) {
              var pos2 = 0;
              while (pos_1 < this.rows) {
                var to_0 = new Position(trs2, pos2);
                if (this.playerNone === this.atPos_dfplqh$(to_0)) {
                  myStones.add_11rb$(new MoveStone(from_0, to_0));
                }
                pos_1 = toByte(pos_1 + 1);
              }
              trs_1 = toByte(trs_1 + 1);
            }
          }
          pos_1 = toByte(pos_1 + 1);
        }
        trs_1 = toByte(trs_1 + 1);
      }
    }
    tmp$_4 = myStones.iterator();
    while (tmp$_4.hasNext()) {
      var myStone = tmp$_4.next();
      var p = myStone.position;
      if (!this.doMove_11rb$(myStone).inMuehle_dfplqh$(p))
        result.add_11rb$(myStone);
      else {
        tmp$_5 = this.stonesToTake_s8j3t7$(this.nextPlayer()).iterator();
        while (tmp$_5.hasNext()) {
          var take = tmp$_5.next();
          result.add_11rb$(new Remove(myStone, take));
        }
      }
    }
    return result;
  };
  Muehle.prototype.inMuehle_dfplqh$ = function (p) {
    var tmp$;
    var c = this.atPos_dfplqh$(p);
    tmp$ = p.rows().iterator();
    while (tmp$.hasNext()) {
      var row = tmp$.next();
      var sum = this.atPos_dfplqh$(row[0]) + this.atPos_dfplqh$(row[1]) + this.atPos_dfplqh$(row[2]) | 0;
      if (sum === 3 * c)
        return true;
    }
    return false;
  };
  Muehle.prototype.stonesToTake_s8j3t7$ = function (pl) {
    var tmp$, tmp$_0;
    var result = ArrayList_init();
    tmp$ = this.columns;
    for (var c = 0; c < tmp$; c++) {
      tmp$_0 = this.rows;
      for (var r = 0; r < tmp$_0; r++) {
        var p = new Position(toByte(c), toByte(r));
        if (this.b[c][r] === pl && !this.inMuehle_dfplqh$(p))
          result.add_11rb$(p);
      }
    }
    return result;
  };
  Muehle.prototype.copy = function () {
    var tmp$, tmp$_0;
    var result = new Muehle();
    result.lastColumn = this.lastColumn;
    result.lastRow = this.lastRow;
    result.movesDone = this.movesDone;
    result.player = this.player;
    result.marked = this.marked == null ? null : new Position(ensureNotNull(this.marked).triblet, ensureNotNull(this.marked).pos);
    result.removing = this.removing;
    tmp$ = this.columns;
    for (var c = 0; c < tmp$; c++) {
      tmp$_0 = this.rows;
      for (var r = 0; r < tmp$_0; r++)
        result.b[c][r] = this.b[c][r];
    }
    result.playerStartStones[0] = this.playerStartStones[0];
    result.playerStartStones[1] = this.playerStartStones[1];
    result.playerStones[0] = this.playerStones[0];
    result.playerStones[1] = this.playerStones[1];
    return result;
  };
  Muehle.prototype.doMove_11rb$ = function (m) {
    var result = m.move_wqgiqk$(this);
    result.prevState = this;
    return result;
  };
  Muehle.prototype.sum_mbbjvw$ = function (a1, a2, b1, b2, c1, c2) {
    return this.b[a1][a2] + this.b[b1][b2] + this.b[c1][c2] | 0;
  };
  Muehle.prototype.evalRow_m6z2d1$ = function (a1, a2, b1, b2, c1, c2, player) {
    var s = this.sum_mbbjvw$(a1, a2, b1, b2, c1, c2);
    if (s === 2 * player + AbstractRegularGame$Companion_getInstance().NONE)
      return 10;
    return s === 2 * this.otherPlayer_s8j3t7$(player) + AbstractRegularGame$Companion_getInstance().NONE ? -10 : 0;
  };
  Muehle.prototype.evalState_s8j3t7$ = function (player) {
    var result = (this.playerStones[player % 2] - this.playerStones[this.otherPlayer_s8j3t7$(player) % 2]) * 1000 | 0;
    result = result + this.evalRow_m6z2d1$(0, 0, 0, 1, 0, 2, player) | 0;
    result = result + this.evalRow_m6z2d1$(0, 2, 0, 3, 0, 4, player) | 0;
    result = result + this.evalRow_m6z2d1$(0, 4, 0, 5, 0, 6, player) | 0;
    result = result + this.evalRow_m6z2d1$(0, 6, 0, 7, 0, 0, player) | 0;
    result = result + this.evalRow_m6z2d1$(1, 0, 1, 1, 1, 2, player) | 0;
    result = result + this.evalRow_m6z2d1$(1, 2, 1, 3, 1, 4, player) | 0;
    result = result + this.evalRow_m6z2d1$(1, 4, 1, 5, 1, 6, player) | 0;
    result = result + this.evalRow_m6z2d1$(1, 6, 1, 7, 1, 0, player) | 0;
    result = result + this.evalRow_m6z2d1$(2, 0, 2, 1, 2, 2, player) | 0;
    result = result + this.evalRow_m6z2d1$(2, 2, 2, 3, 2, 4, player) | 0;
    result = result + this.evalRow_m6z2d1$(2, 4, 2, 5, 2, 6, player) | 0;
    result = result + this.evalRow_m6z2d1$(2, 6, 2, 7, 2, 0, player) | 0;
    result = result + this.evalRow_m6z2d1$(0, 1, 1, 1, 2, 1, player) | 0;
    result = result + this.evalRow_m6z2d1$(0, 3, 1, 3, 2, 3, player) | 0;
    result = result + this.evalRow_m6z2d1$(0, 5, 1, 5, 2, 5, player) | 0;
    result = result + this.evalRow_m6z2d1$(0, 7, 1, 7, 2, 7, player) | 0;
    return result;
  };
  Muehle.prototype.wins_s8j3t7$ = function (player) {
    return this.playerStones[player % 2] === 2;
  };
  Muehle.prototype.noMoreMove = function () {
    return this.legalMoves().isEmpty();
  };
  Muehle.prototype.toString = function () {
    var tmp$;
    var result = '';
    for (var r = this.rows - 1 | 0; r >= 0; r--) {
      tmp$ = this.columns;
      for (var c = 0; c < tmp$; c++)
        result + ('' + toString(this.b[c][r]));
      result + '\n';
    }
    return result + '';
  };
  Muehle.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Muehle',
    interfaces: [AbstractRegularGame]
  };
  function MuehleGUI() {
    this.game = new Muehle();
    this.div = document.createElement('div');
    var array = Array_0(this.game.columns);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      var array_0 = Array_0(this.game.rows);
      var tmp$_0;
      tmp$_0 = array_0.length - 1 | 0;
      for (var i_0 = 0; i_0 <= tmp$_0; i_0++) {
        array_0[i_0] = new MuehleGUI$Field(this, 0, 0);
      }
      array[i] = array_0;
    }
    this.fs = array;
  }
  MuehleGUI.prototype.init = function () {
    var tmp$, tmp$_0, tmp$_1, tmp$_2, tmp$_3, tmp$_4;
    var empty = document.getElementsByClassName('empty');
    tmp$ = empty.length;
    for (var i = 0; i < tmp$; i++) {
      ensureNotNull(empty[i]).setAttribute('width', (80).toString());
      ensureNotNull(empty[i]).setAttribute('height', (80).toString());
      appendText(ensureNotNull(empty[i]), ' ');
    }
    var field = document.getElementsByClassName('field');
    tmp$_0 = field.length;
    for (var i_0 = 0; i_0 < tmp$_0; i_0++) {
      ensureNotNull(field[i_0]).setAttribute('width', (80).toString());
      ensureNotNull(field[i_0]).setAttribute('height', (80).toString());
    }
    var horiz = document.getElementsByClassName('horiz');
    tmp$_1 = horiz.length;
    for (var i_1 = 0; i_1 < tmp$_1; i_1++) {
      ensureNotNull(horiz[i_1]).setAttribute('width', (80).toString());
      ensureNotNull(horiz[i_1]).setAttribute('height', (80).toString());
      var horiz1 = new MuehleGUI$Verti(this);
      ensureNotNull(horiz[i_1]).appendChild(horiz1.canvas);
      horiz1.paint();
    }
    var verti = document.getElementsByClassName('verti');
    tmp$_2 = verti.length;
    for (var i_2 = 0; i_2 < tmp$_2; i_2++) {
      ensureNotNull(verti[i_2]).setAttribute('width', (80).toString());
      ensureNotNull(verti[i_2]).setAttribute('height', (80).toString());
      var horiz1_0 = new MuehleGUI$Horiz(this);
      ensureNotNull(verti[i_2]).appendChild(horiz1_0.canvas);
      horiz1_0.paint();
    }
    tmp$_3 = this.fs.length;
    for (var c = 0; c < tmp$_3; c++) {
      tmp$_4 = this.fs[c].length;
      for (var f = 0; f < tmp$_4; f++) {
        this.fs[c][f] = new MuehleGUI$Field(this, toByte(c), toByte(f));
        ensureNotNull(document.getElementById(f.toString() + c)).appendChild(this.fs[c][f].canvas);
        this.fs[c][f].paint();
      }
    }
  };
  MuehleGUI.prototype.paint = function () {
    var tmp$, tmp$_0, tmp$_1;
    tmp$ = this.fs;
    for (tmp$_0 = 0; tmp$_0 !== tmp$.length; ++tmp$_0) {
      var c = tmp$[tmp$_0];
      for (tmp$_1 = 0; tmp$_1 !== c.length; ++tmp$_1) {
        var f = c[tmp$_1];
        f.paint();
      }
    }
  };
  function MuehleGUI$Verti($outer, canvas) {
    this.$outer = $outer;
    var tmp$;
    if (canvas === void 0)
      canvas = Kotlin.isType(tmp$ = document.createElement('canvas'), HTMLCanvasElement) ? tmp$ : throwCCE();
    this.canvas = canvas;
    this.canvas.height = 80;
    this.canvas.width = 80;
  }
  MuehleGUI$Verti.prototype.paint = function () {
    var tmp$;
    var ctx = Kotlin.isType(tmp$ = this.canvas.getContext('2d'), CanvasRenderingContext2D) ? tmp$ : throwCCE();
    ctx.fillRect(0.0, 30.0, 80.0, 20.0);
    ctx.stroke();
  };
  MuehleGUI$Verti.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Verti',
    interfaces: []
  };
  function MuehleGUI$Horiz($outer, canvas) {
    this.$outer = $outer;
    var tmp$;
    if (canvas === void 0)
      canvas = Kotlin.isType(tmp$ = document.createElement('canvas'), HTMLCanvasElement) ? tmp$ : throwCCE();
    this.canvas = canvas;
    this.canvas.height = 80;
    this.canvas.width = 80;
  }
  MuehleGUI$Horiz.prototype.paint = function () {
    var tmp$;
    var ctx = Kotlin.isType(tmp$ = this.canvas.getContext('2d'), CanvasRenderingContext2D) ? tmp$ : throwCCE();
    ctx.fillRect(30.0, 0.0, 20.0, 80.0);
    ctx.stroke();
  };
  MuehleGUI$Horiz.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Horiz',
    interfaces: []
  };
  function MuehleGUI$Field($outer, c, r, canvas) {
    this.$outer = $outer;
    var tmp$;
    if (canvas === void 0)
      canvas = Kotlin.isType(tmp$ = document.createElement('canvas'), HTMLCanvasElement) ? tmp$ : throwCCE();
    this.c = c;
    this.r = r;
    this.canvas = canvas;
    this.canvas.height = 80;
    this.canvas.width = 80;
    this.canvas.addEventListener('click', MuehleGUI$MuehleGUI$Field_init$lambda(this, this.$outer));
  }
  MuehleGUI$Field.prototype.paint = function () {
    var tmp$, tmp$_0, tmp$_1;
    var ctx = Kotlin.isType(tmp$ = this.canvas.getContext('2d'), CanvasRenderingContext2D) ? tmp$ : throwCCE();
    ctx.beginPath();
    ctx.arc(40.0, 40.0, 40.0, 0.0, 2 * math.PI);
    tmp$_0 = this.$outer.game.getAtPosition_5gdoe6$(this.c, this.r);
    if (tmp$_0 === this.$outer.game.playerNone)
      tmp$_1 = '#000000';
    else if (tmp$_0 === this.$outer.game.playerOne)
      tmp$_1 = '#FF0000';
    else
      tmp$_1 = '#0000FF';
    ctx.fillStyle = tmp$_1;
    ctx.fill();
    ctx.stroke();
  };
  function MuehleGUI$MuehleGUI$Field_init$lambda(this$Field, this$MuehleGUI) {
    return function (it) {
      println('clicked: ' + toString(this$Field.c) + ' ' + toString(this$Field.r));
      var m = this$MuehleGUI.game.createMove_5gdoe6$(this$Field.c, this$Field.r);
      println(m);
      println(this$MuehleGUI.game.legalMoves());
      if (m != null) {
        println('was legal');
        this$MuehleGUI.game = this$MuehleGUI.game.doMove_11rb$(m);
        println('moved');
      }
      println(this$MuehleGUI);
      this$MuehleGUI.paint();
      return Unit;
    };
  }
  MuehleGUI$Field.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Field',
    interfaces: []
  };
  MuehleGUI.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'MuehleGUI',
    interfaces: []
  };
  function startMuehleGUI() {
    (new MuehleGUI()).init();
  }
  function OthelloMove(mv, turn) {
    this.mv = mv;
    this.turn = turn;
  }
  OthelloMove.prototype.toString = function () {
    return 'OthelloMove(' + toString(this.mv) + ',' + toString(this.turn) + ')';
  };
  OthelloMove.prototype.equals = function (other) {
    return other != null && Kotlin.isType(other, OthelloMove) && other.mv.equals(this.mv);
  };
  OthelloMove.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'OthelloMove',
    interfaces: []
  };
  function NoMove() {
    OthelloMove.call(this, new Pair(toByte(-1), toByte(-1)), emptyList());
  }
  NoMove.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'NoMove',
    interfaces: [OthelloMove]
  };
  function Othello() {
    AbstractRegularGame.call(this, 8, 8);
    this.b[3][3] = this.playerOne;
    this.b[4][4] = this.playerOne;
    this.b[3][4] = this.playerTwo;
    this.b[4][3] = this.playerTwo;
    this.movesDone = 4;
  }
  Othello.prototype.createMove_5gdoe6$ = function (column, row) {
    return this.mkMove_blxopz$(new Pair(column, row));
  };
  Othello.prototype.copy = function () {
    var tmp$, tmp$_0;
    var result = new Othello();
    result.lastColumn = this.lastColumn;
    result.lastRow = this.lastRow;
    result.movesDone = this.movesDone;
    result.player = this.player;
    tmp$ = this.columns;
    for (var c = 0; c < tmp$; c++) {
      tmp$_0 = this.rows;
      for (var r = 0; r < tmp$_0; r++)
        result.b[c][r] = this.b[c][r];
    }
    return result;
  };
  Othello.prototype.noMoreMove = function () {
    return this.movesDone === 64;
  };
  Othello.prototype.wins_s8j3t7$ = function (player) {
    return this.noMoreMove() && this.evalState_s8j3t7$(player) > 32;
  };
  Othello.prototype.ended = function () {
    return this.noMoreMove();
  };
  Othello.prototype.evalState_s8j3t7$ = function (player) {
    var tmp$, tmp$_0;
    var result = 0;
    tmp$ = this.columns;
    for (var trs = 0; trs < tmp$; trs++) {
      tmp$_0 = this.rows;
      for (var pos = 0; pos < tmp$_0; pos++)
        if (this.b[trs][pos] === player)
          result = result + 1 | 0;
    }
    return result;
  };
  Othello.prototype.toTurn_blxopz$ = function (m) {
    var result = ArrayList_init();
    if (m.first + 2 < this.columns) {
      if (this.b[m.first + 1][m.second] === this.nextPlayer()) {
        var i = m.first + 2;
        while (i < this.columns && this.b[i][m.second] !== AbstractRegularGame$Companion_getInstance().NONE && this.b[i][m.second] !== this.player) {
          i = i + 1 | 0;
        }
        if (i < this.columns && this.b[i][m.second] === this.player) {
          i = i - 1 | 0;
          while (i > m.first) {
            result.add_11rb$(new Pair(toByte(i), m.second));
            i = i - 1 | 0;
          }
        }
      }
    }
    if (m.first - 2 >= 0) {
      if (this.b[m.first - 1][m.second] === this.nextPlayer()) {
        var i_0 = m.first - 2;
        while (i_0 >= 0 && this.b[i_0][m.second] !== AbstractRegularGame$Companion_getInstance().NONE && this.b[i_0][m.second] !== this.player) {
          i_0 = i_0 - 1 | 0;
        }
        if (i_0 >= 0 && this.b[i_0][m.second] === this.player) {
          i_0 = i_0 + 1 | 0;
          while (i_0 < m.first) {
            result.add_11rb$(new Pair(toByte(i_0), m.second));
            i_0 = i_0 + 1 | 0;
          }
        }
      }
    }
    if (m.second + 2 < this.rows) {
      if (this.b[m.first][m.second + 1] === this.nextPlayer()) {
        var i_1 = m.second + 2;
        while (i_1 < this.rows && this.b[m.first][i_1] !== AbstractRegularGame$Companion_getInstance().NONE && this.b[m.first][i_1] !== this.player) {
          i_1 = i_1 + 1 | 0;
        }
        if (i_1 < this.rows && this.b[m.first][i_1] === this.player) {
          i_1 = i_1 - 1 | 0;
          while (i_1 > m.second) {
            result.add_11rb$(new Pair(m.first, toByte(i_1)));
            i_1 = i_1 - 1 | 0;
          }
        }
      }
    }
    if (m.second - 2 >= 0) {
      if (this.b[m.first][m.second - 1] === this.nextPlayer()) {
        var i_2 = m.second - 2;
        while (i_2 >= 0 && this.b[m.first][i_2] !== AbstractRegularGame$Companion_getInstance().NONE && this.b[m.first][i_2] !== this.player) {
          i_2 = i_2 - 1 | 0;
        }
        if (i_2 >= 0 && this.b[m.first][i_2] === this.player) {
          i_2 = i_2 + 1 | 0;
          while (i_2 < m.second) {
            result.add_11rb$(new Pair(m.first, toByte(i_2)));
            i_2 = i_2 + 1 | 0;
          }
        }
      }
    }
    if (m.first + 2 < this.columns && m.second + 2 < this.rows) {
      if (this.b[m.first + 1][m.second + 1] === this.nextPlayer()) {
        var i_3 = toByte(2);
        while (m.first + i_3 < this.columns && m.second + i_3 < this.rows && this.b[m.first + i_3][m.second + i_3] !== AbstractRegularGame$Companion_getInstance().NONE && this.b[m.first + i_3][m.second + i_3] !== this.player) {
          i_3 = toByte(i_3 + 1);
        }
        if (m.first + i_3 < this.columns && m.second + i_3 < this.rows && this.b[m.first + i_3][m.second + i_3] === this.player) {
          i_3 = toByte(i_3 - 1);
          while (i_3 > 0) {
            result.add_11rb$(new Pair(toByte(m.first + i_3), toByte(m.second + i_3)));
            i_3 = toByte(i_3 - 1);
          }
        }
      }
    }
    if (m.first - 2 >= 0 && m.second - 2 >= 0) {
      if (this.b[m.first - 1][m.second - 1] === this.nextPlayer()) {
        var i_4 = 2;
        while (m.first - i_4 >= 0 && m.second - i_4 >= 0 && this.b[m.first - i_4][m.second - i_4] !== AbstractRegularGame$Companion_getInstance().NONE && this.b[m.first - i_4][m.second - i_4] !== this.player) {
          i_4 = i_4 + 1 | 0;
        }
        if (m.first - i_4 >= 0 && m.second - i_4 >= 0 && this.b[m.first - i_4][m.second - i_4] === this.player) {
          i_4 = i_4 - 1 | 0;
          while (i_4 > 0) {
            result.add_11rb$(new Pair(toByte(m.first - i_4), toByte(m.second - i_4)));
            i_4 = i_4 - 1 | 0;
          }
        }
      }
    }
    if (m.first + 2 < this.columns && m.second - 2 >= 0) {
      if (this.b[m.first + 1][m.second - 1] === this.nextPlayer()) {
        var i_5 = 2;
        while (m.first + i_5 < this.columns && m.second - i_5 >= 0 && this.b[m.first + i_5][m.second - i_5] !== AbstractRegularGame$Companion_getInstance().NONE && this.b[m.first + i_5][m.second - i_5] !== this.player) {
          i_5 = i_5 + 1 | 0;
        }
        if (m.first + i_5 < this.columns && m.second - i_5 >= 0 && this.b[m.first + i_5][m.second - i_5] === this.player) {
          i_5 = i_5 - 1 | 0;
          while (i_5 > 0) {
            result.add_11rb$(new Pair(toByte(m.first + i_5), toByte(m.second - i_5)));
            i_5 = i_5 - 1 | 0;
          }
        }
      }
    }
    if (m.first - 2 >= 0 && m.second + 2 < this.rows) {
      if (this.b[m.first - 1][m.second + 1] === this.nextPlayer()) {
        var i_6 = 2;
        while (m.first - i_6 >= 0 && m.second + i_6 < this.rows && this.b[m.first - i_6][m.second + i_6] !== AbstractRegularGame$Companion_getInstance().NONE && this.b[m.first - i_6][m.second + i_6] !== this.player) {
          i_6 = i_6 + 1 | 0;
        }
        if (m.first - i_6 >= 0 && m.second + i_6 < this.rows && this.b[m.first - i_6][m.second + i_6] === this.player) {
          i_6 = i_6 - 1 | 0;
          while (i_6 > 0) {
            result.add_11rb$(new Pair(toByte(m.first - i_6), toByte(m.second + i_6)));
            i_6 = i_6 - 1 | 0;
          }
        }
      }
    }
    return result;
  };
  Othello.prototype.mkMove_blxopz$ = function (m) {
    if (this.b[m.first][m.second] === AbstractRegularGame$Companion_getInstance().NONE) {
      var turn = this.toTurn_blxopz$(m);
      if (!turn.isEmpty())
        return new OthelloMove(m, turn);
    }
    return new NoMove();
  };
  Othello.prototype.doMove_11rb$ = function (m) {
    var tmp$;
    var om = m.mv;
    var result = this.copy();
    result.player = this.nextPlayer();
    if (Kotlin.isType(m, NoMove))
      return result;
    result.b[om.first][om.second] = this.player;
    tmp$ = m.turn.iterator();
    while (tmp$.hasNext()) {
      var t = tmp$.next();
      result.b[t.first][t.second] = this.player;
    }
    result.movesDone = this.movesDone + 1 | 0;
    return result;
  };
  Othello.prototype.setAtPosition_5gdoe6$ = function (column, row) {
    return this.doMove_11rb$(this.mkMove_blxopz$(new Pair(column, row)));
  };
  Othello.prototype.legalMoves = function () {
    var tmp$, tmp$_0;
    var result = ArrayList_init();
    tmp$ = this.columns;
    for (var trs = 0; trs < tmp$; trs++) {
      tmp$_0 = this.rows;
      for (var pos = 0; pos < tmp$_0; pos++) {
        var m = new Pair(toByte(trs), toByte(pos));
        if (this.b[trs][pos] === AbstractRegularGame$Companion_getInstance().NONE) {
          var turn = this.toTurn_blxopz$(m);
          if (!turn.isEmpty())
            result.add_11rb$(new OthelloMove(m, turn));
        }
      }
    }
    if (result.isEmpty())
      result.add_11rb$(new NoMove());
    return result;
  };
  Othello.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Othello',
    interfaces: [AbstractRegularGame]
  };
  function Game() {
  }
  Game.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'Game',
    interfaces: []
  };
  function RegularGame() {
  }
  RegularGame.$metadata$ = {
    kind: Kind_INTERFACE,
    simpleName: 'RegularGame',
    interfaces: [Game]
  };
  function AbstractRegularGame(columns, rows) {
    AbstractRegularGame$Companion_getInstance();
    this.columns_l44xpb$_0 = columns;
    this.rows_jxdbz5$_0 = rows;
    this.playerOne_oyt2h5$_0 = AbstractRegularGame$Companion_getInstance().ONE;
    this.playerTwo_oypomn$_0 = AbstractRegularGame$Companion_getInstance().TWO;
    this.playerNone_7coj3z$_0 = AbstractRegularGame$Companion_getInstance().NONE;
    this.player = AbstractRegularGame$Companion_getInstance().ONE;
    var array = Array_0(this.columns);
    var tmp$;
    tmp$ = array.length - 1 | 0;
    for (var i = 0; i <= tmp$; i++) {
      array[i] = new Int8Array(this.rows);
    }
    this.b = array;
    this.movesDone = 0;
    this.lastColumn = -1 | 0;
    this.lastRow = -1 | 0;
    this.winsLast = false;
  }
  Object.defineProperty(AbstractRegularGame.prototype, 'columns', {
    get: function () {
      return this.columns_l44xpb$_0;
    }
  });
  Object.defineProperty(AbstractRegularGame.prototype, 'rows', {
    get: function () {
      return this.rows_jxdbz5$_0;
    }
  });
  Object.defineProperty(AbstractRegularGame.prototype, 'playerOne', {
    get: function () {
      return this.playerOne_oyt2h5$_0;
    }
  });
  Object.defineProperty(AbstractRegularGame.prototype, 'playerTwo', {
    get: function () {
      return this.playerTwo_oypomn$_0;
    }
  });
  Object.defineProperty(AbstractRegularGame.prototype, 'playerNone', {
    get: function () {
      return this.playerNone_7coj3z$_0;
    }
  });
  AbstractRegularGame.prototype.currentPlayer = function () {
    return this.player;
  };
  AbstractRegularGame.prototype.interestingMoves = function () {
    return this.legalMoves();
  };
  AbstractRegularGame.prototype.otherPlayer_s8j3t7$ = function (p) {
    return p === AbstractRegularGame$Companion_getInstance().ONE ? AbstractRegularGame$Companion_getInstance().TWO : AbstractRegularGame$Companion_getInstance().ONE;
  };
  AbstractRegularGame.prototype.nextPlayer = function () {
    return this.otherPlayer_s8j3t7$(this.player);
  };
  AbstractRegularGame.prototype.lastPlayer = function () {
    return this.otherPlayer_s8j3t7$(this.player);
  };
  AbstractRegularGame.prototype.getAtPosition_5gdoe6$ = function (column, row) {
    return this.b[column][row];
  };
  AbstractRegularGame.prototype.wins = function () {
    this.winsLast = this.wins_s8j3t7$(this.lastPlayer());
    return this.winsLast;
  };
  AbstractRegularGame.prototype.ended = function () {
    return this.noMoreMove() || this.wins();
  };
  function AbstractRegularGame$Companion() {
    AbstractRegularGame$Companion_instance = this;
    this.NONE = 0;
    this.ONE = 1;
    this.TWO = 2;
  }
  AbstractRegularGame$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var AbstractRegularGame$Companion_instance = null;
  function AbstractRegularGame$Companion_getInstance() {
    if (AbstractRegularGame$Companion_instance === null) {
      new AbstractRegularGame$Companion();
    }
    return AbstractRegularGame$Companion_instance;
  }
  AbstractRegularGame.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'AbstractRegularGame',
    interfaces: [RegularGame]
  };
  function TicTacToe() {
    AbstractRegularGame.call(this, 3, 3);
  }
  TicTacToe.prototype.createMove_5gdoe6$ = function (column, row) {
    return new Pair(column, row);
  };
  TicTacToe.prototype.legalMoves = function () {
    var tmp$, tmp$_0;
    var result = ArrayList_init();
    tmp$ = this.columns;
    for (var c = 0; c < tmp$; c++) {
      tmp$_0 = this.rows;
      for (var r = 0; r < tmp$_0; r++)
        if (this.b[c][r] === AbstractRegularGame$Companion_getInstance().NONE)
          result.add_11rb$(new Pair(toByte(c), toByte(r)));
    }
    return result;
  };
  TicTacToe.prototype.doMove_11rb$ = function (m) {
    var result = this.copy();
    result.player = this.nextPlayer();
    result.b[m.first][m.second] = this.player;
    result.movesDone = toByte(this.movesDone + 1 | 0);
    return result;
  };
  TicTacToe.prototype.noMoreMove = function () {
    return Kotlin.imul(this.rows, this.columns) === this.movesDone;
  };
  TicTacToe.prototype.wins_s8j3t7$ = function (player) {
    return this.checkRows_0(player) || this.checkColumns_0(player) || this.checkDiagonal1_0(player) || this.checkDiagonal2_0(player);
  };
  TicTacToe.prototype.checkRows_0 = function (p) {
    var tmp$, tmp$_0;
    tmp$ = this.rows;
    for (var r = 0; r < tmp$; r++) {
      tmp$_0 = this.columns;
      for (var c = 0; c < tmp$_0; c++) {
        if (this.b[c][r] !== p)
          break;
        if (c === (this.columns - 1 | 0))
          return true;
      }
    }
    return false;
  };
  TicTacToe.prototype.checkColumns_0 = function (p) {
    var tmp$, tmp$_0;
    tmp$ = this.columns;
    for (var c = 0; c < tmp$; c++) {
      tmp$_0 = this.rows;
      for (var r = 0; r < tmp$_0; r++) {
        if (this.b[c][r] !== p)
          break;
        if (r === (this.rows - 1 | 0))
          return true;
      }
    }
    return false;
  };
  TicTacToe.prototype.checkDiagonal1_0 = function (p) {
    var tmp$;
    tmp$ = this.rows;
    for (var r = 0; r < tmp$; r++) {
      if (this.b[r][r] !== p)
        break;
      if (r === (this.rows - 1 | 0))
        return true;
    }
    return false;
  };
  TicTacToe.prototype.checkDiagonal2_0 = function (p) {
    var tmp$;
    tmp$ = this.rows;
    for (var r = 0; r < tmp$; r++) {
      if (this.b[r][this.rows - r - 1 | 0] !== p)
        break;
      if (r === (this.rows - 1 | 0))
        return true;
    }
    return false;
  };
  TicTacToe.prototype.evalState_s8j3t7$ = function (player) {
    return this.wins() ? this.lastPlayer() === player ? kotlin_js_internal_IntCompanionObject.MAX_VALUE : -2147483647 : 0;
  };
  TicTacToe.prototype.copy = function () {
    var tmp$, tmp$_0;
    var result = new TicTacToe();
    result.lastColumn = this.lastColumn;
    result.lastRow = this.lastRow;
    result.movesDone = this.movesDone;
    result.player = this.player;
    tmp$ = this.columns;
    for (var c = 0; c < tmp$; c++) {
      tmp$_0 = this.rows;
      for (var r = 0; r < tmp$_0; r++)
        result.b[c][r] = this.b[c][r];
    }
    return result;
  };
  TicTacToe.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'TicTacToe',
    interfaces: [AbstractRegularGame]
  };
  function Vier() {
    Vier$Companion_getInstance();
    AbstractRegularGame.call(this, 7, 6);
  }
  Vier.prototype.createMove_5gdoe6$ = function (column, row) {
    return column;
  };
  Vier.prototype.interestingMoves = function () {
    var tmp$;
    var result = ArrayList_init();
    var pms = this.movesDone === 0 || (this.movesDone === 1 && this.lastColumn >= 2 && this.lastColumn <= 4) ? Vier$Companion_getInstance().startMoves : Vier$Companion_getInstance().possibleMoves;
    for (tmp$ = 0; tmp$ !== pms.length; ++tmp$) {
      var c = pms[tmp$];
      if (this.b[c][this.rows - 1 | 0] === AbstractRegularGame$Companion_getInstance().NONE)
        result.add_11rb$(c);
    }
    return result;
  };
  Vier.prototype.legalMoves = function () {
    var tmp$;
    var result = ArrayList_init();
    var pms = Vier$Companion_getInstance().possibleMoves;
    for (tmp$ = 0; tmp$ !== pms.length; ++tmp$) {
      var c = pms[tmp$];
      if (this.b[c][this.rows - 1 | 0] === AbstractRegularGame$Companion_getInstance().NONE)
        result.add_11rb$(c);
    }
    return result;
  };
  Vier.prototype.doMove_11rb$ = function (m) {
    var tmp$;
    var result = this.copy();
    tmp$ = this.rows;
    for (var r = 0; r < tmp$; r++) {
      if (result.b[m][r] === AbstractRegularGame$Companion_getInstance().NONE) {
        result.b[m][r] = this.player;
        result.player = this.nextPlayer();
        result.lastColumn = m;
        result.lastRow = toByte(r);
        result.movesDone = this.movesDone + 1 | 0;
        break;
      }
    }
    return result;
  };
  Vier.prototype.noMoreMove = function () {
    return this.movesDone === Kotlin.imul(this.rows, this.columns);
  };
  Vier.prototype.wins_s8j3t7$ = function (player) {
    if (this.movesDone < 7)
      return false;
    return this.b[this.lastColumn][this.lastRow] !== player ? false : this.vertical_0(player) || this.horizontal_0(player) || this.diagonal1_0(player) || this.diagonal2_0(player);
  };
  Vier.prototype.vertical_0 = function (p) {
    var inRow = 1;
    var cr = this.lastRow - 1;
    while (cr >= 0 && this.b[this.lastColumn][cr] === p) {
      inRow = toByte(inRow + 1);
      cr = cr - 1 | 0;
    }
    return inRow >= 4;
  };
  Vier.prototype.horizontal_0 = function (p) {
    var inRow = {v: 1};
    var cc = this.lastColumn - 1;
    while (cc >= 0 && this.b[cc][this.lastRow] === p) {
      inRow.v = toByte(inRow.v + 1);
      cc = cc - 1 | 0;
    }
    var cc_0 = this.lastColumn + 1;
    while (cc_0 < this.columns && this.b[cc_0][this.lastRow] === p) {
      inRow.v = toByte(inRow.v + 1);
      cc_0 = cc_0 + 1 | 0;
    }
    return inRow.v >= 4;
  };
  Vier.prototype.diagonal1_0 = function (p) {
    var inRow = 1;
    var cc = toByte(this.lastColumn - 1);
    var cr = toByte(this.lastRow - 1);
    while (cc >= 0 && cr >= 0 && this.b[cc][cr] === p) {
      inRow = toByte(inRow + 1);
      cc = toByte(cc - 1);
      cr = toByte(cr - 1);
    }
    cc = toByte(this.lastColumn + 1);
    cr = toByte(this.lastRow + 1);
    while (cc < this.columns && cr < this.rows && this.b[cc][cr] === p) {
      inRow = toByte(inRow + 1);
      cc = toByte(cc + 1);
      cr = toByte(cr + 1);
    }
    return inRow >= 4;
  };
  Vier.prototype.diagonal2_0 = function (p) {
    var inRow = 1;
    var cc = toByte(this.lastColumn - 1);
    var cr = toByte(this.lastRow + 1);
    while (cc >= 0 && cr < this.rows && this.b[cc][cr] === p) {
      inRow = toByte(inRow + 1);
      cc = toByte(cc - 1);
      cr = toByte(cr + 1);
    }
    cc = toByte(this.lastColumn + 1);
    cr = toByte(this.lastRow - 1);
    while (cc < this.columns && cr >= 0 && this.b[cc][cr] === p) {
      inRow = toByte(inRow + 1);
      cc = toByte(cc + 1);
      cr = toByte(cr - 1);
    }
    return inRow >= 4;
  };
  Vier.prototype.evalState_s8j3t7$ = function (player) {
    return this.wins() ? this.lastPlayer() === player ? kotlin_js_internal_IntCompanionObject.MAX_VALUE : -2147483647 : this.state_s8j3t7$(player) - (10 * this.state_s8j3t7$(this.otherPlayer_s8j3t7$(player)) | 0) | 0;
  };
  Vier.prototype.ps_pdp8qh$ = function (player, rV) {
    return rV === 3 * player ? 100 : rV === 2 * player ? 10 : 0;
  };
  Vier.prototype.state_s8j3t7$ = function (p) {
    var tmp$, tmp$_0;
    var result = 0;
    tmp$ = this.columns - 3 | 0;
    for (var c = 0; c < tmp$; c++) {
      tmp$_0 = this.rows - 3 | 0;
      for (var r = 0; r < tmp$_0; r++) {
        result = result + this.ps_pdp8qh$(p, this.b[c][r] + this.b[c][r + 1 | 0] + this.b[c][r + 2 | 0] + this.b[c][r + 3 | 0] | 0) + this.ps_pdp8qh$(p, this.b[c][r] + this.b[c + 1 | 0][r + 1 | 0] + this.b[c + 2 | 0][r + 2 | 0] + this.b[c + 3 | 0][r + 3 | 0] | 0) + this.ps_pdp8qh$(p, this.b[c][r] + this.b[c + 1 | 0][r] + this.b[c + 2 | 0][r] + this.b[c + 3 | 0][r] | 0) | 0;
        var c2 = this.columns - 1 - c | 0;
        if (c2 > 3)
          result = result + this.ps_pdp8qh$(p, this.b[c2][r] + this.b[c2][r + 1 | 0] + this.b[c2][r + 2 | 0] + this.b[c2][r + 3 | 0] | 0) | 0;
        var s = this.rows - 1 - r | 0;
        result = result + this.ps_pdp8qh$(p, this.b[c][s] + this.b[c + 1 | 0][s] + this.b[c + 2 | 0][s] + this.b[c + 3 | 0][s] | 0) + this.ps_pdp8qh$(p, this.b[c][s] + this.b[c + 1 | 0][s - 1 | 0] + this.b[c + 2 | 0][s - 2 | 0] + this.b[c + 3 | 0][s - 3 | 0] | 0) | 0;
      }
    }
    return result;
  };
  Vier.prototype.copy = function () {
    var tmp$, tmp$_0;
    var result = new Vier();
    result.lastColumn = this.lastColumn;
    result.lastRow = this.lastRow;
    result.movesDone = this.movesDone;
    result.player = this.player;
    tmp$ = this.columns;
    for (var c = 0; c < tmp$; c++) {
      tmp$_0 = this.rows;
      for (var r = 0; r < tmp$_0; r++) {
        result.b[c][r] = this.b[c][r];
      }
    }
    return result;
  };
  function Vier$Companion() {
    Vier$Companion_instance = this;
    this.possibleMoves = new Int8Array([3, 2, 4, 1, 5, 0, 6]);
    this.startMoves = new Int8Array([3, 2, 4]);
  }
  Vier$Companion.$metadata$ = {
    kind: Kind_OBJECT,
    simpleName: 'Companion',
    interfaces: []
  };
  var Vier$Companion_instance = null;
  function Vier$Companion_getInstance() {
    if (Vier$Companion_instance === null) {
      new Vier$Companion();
    }
    return Vier$Companion_instance;
  }
  Vier.$metadata$ = {
    kind: Kind_CLASS,
    simpleName: 'Vier',
    interfaces: [AbstractRegularGame]
  };
  _.negaMax_967imi$ = negaMax;
  _.bestMove_qkom2e$ = bestMove;
  GameBoard.Field = GameBoard$Field;
  _.GameBoard = GameBoard;
  _.viergewinnt = viergewinnt;
  _.tictactoe = tictactoe;
  _.othello = othello;
  _.Move = Move;
  _.Position = Position;
  _.SetStone = SetStone;
  _.MoveStone = MoveStone;
  _.Set_init_dfplqh$ = Set_init;
  _.Set_init_5gdoe6$ = Set_init_0;
  _.Set = Set;
  _.Remove = Remove;
  _.RemoveStone_init_dfplqh$ = RemoveStone_init;
  _.RemoveStone_init_5gdoe6$ = RemoveStone_init_0;
  _.RemoveStone = RemoveStone;
  _.Muehle = Muehle;
  MuehleGUI.Verti = MuehleGUI$Verti;
  MuehleGUI.Horiz = MuehleGUI$Horiz;
  MuehleGUI.Field = MuehleGUI$Field;
  _.MuehleGUI = MuehleGUI;
  _.startMuehleGUI = startMuehleGUI;
  _.OthelloMove = OthelloMove;
  _.NoMove = NoMove;
  _.Othello = Othello;
  _.Game = Game;
  _.RegularGame = RegularGame;
  Object.defineProperty(AbstractRegularGame, 'Companion', {
    get: AbstractRegularGame$Companion_getInstance
  });
  _.AbstractRegularGame = AbstractRegularGame;
  _.TicTacToe = TicTacToe;
  Object.defineProperty(Vier, 'Companion', {
    get: Vier$Companion_getInstance
  });
  _.Vier = Vier;
  Kotlin.defineModule('strategicGames', _);
  return _;
}(typeof strategicGames === 'undefined' ? {} : strategicGames, kotlin);
