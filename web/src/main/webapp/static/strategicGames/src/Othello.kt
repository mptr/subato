open class OthelloMove(var mv: Pair<Byte, Byte>, var turn: List<Pair<Byte, Byte>>){
    override fun toString(): String {
        return "OthelloMove("+mv+","+turn+")"
    }

    override fun equals(other: Any?): Boolean {
        return other!=null && other is OthelloMove && other.mv.equals(mv)
    }
}

class NoMove : OthelloMove(Pair<Byte,Byte>((-1).toByte(),(-1).toByte()), emptyList())


class Othello : AbstractRegularGame<OthelloMove>(8, 8) {
    override fun createMove(column: Byte, row: Byte): OthelloMove {

        return mkMove(Pair(column,row))
    }

    fun copy():Othello{
        val result =  Othello();
        result.lastColumn = lastColumn;
        result.lastRow=lastRow
        result.movesDone=movesDone
        result.player=player
        for (c in 0 until columns)
            for (r in 0 until rows)
                result.b[c][r] = b[c][r]
        return result;
    }


    init {
        b[3][3] = playerOne
        b[4][4] = playerOne
        b[3][4] = playerTwo
        b[4][3] = playerTwo
        movesDone = 4
    }

    override fun noMoreMove(): Boolean {
        return movesDone == 64
    }

    override fun wins(player: Byte): Boolean {
        return noMoreMove() && evalState(player) > 32
    }

    override fun ended(): Boolean {
        return noMoreMove()
    }

    override fun evalState(player: Byte): Int {
        var result = 0
        for (trs in 0 until columns)
            for (pos in 0 until rows)
                if (b[trs][pos] == player) result = result + 1
        return result
    }

    fun toTurn(m: Pair<Byte, Byte>): List<Pair<Byte, Byte>> {
        var result =  mutableListOf<Pair<Byte, Byte>>()
        if (m.first + 2 < columns) {
            if (b[m.first + 1][m.second.toInt()] == nextPlayer()) {
                var i = (m.first + 2)
                while (i < columns && b[i.toInt()][m.second.toInt()] != AbstractRegularGame.NONE && b[i.toInt()][m.second.toInt()] != player)
                    i++
                if (i < columns && b[i.toInt()][m.second.toInt()] == player) {
                    i = (i - 1)
                    while (i > m.first) {
                        result.add(Pair<Byte, Byte>(i.toByte(), m.second))
                        i--
                    }
                }
            }
        }
        if (m.first - 2 >= 0) {
            if (b[m.first - 1][m.second.toInt()] == nextPlayer()) {
                var i = (m.first - 2)
                while (i >= 0 && b[i][m.second.toInt()] != AbstractRegularGame.NONE && b[i][m.second.toInt()] != player)
                    i--
                if (i >= 0 && b[i][m.second.toInt()] == player) {
                    i = (i + 1)
                    while (i < m.first) {
                        result.add(Pair<Byte, Byte>(i.toByte(), m.second))
                        i++
                    }
                }
            }
        }
        if (m.second + 2 < rows) {
            if (b[m.first.toInt()][m.second + 1] == nextPlayer()) {
                var i = (m.second + 2)
                while (i < rows && b[m.first.toInt()][i] != AbstractRegularGame.NONE && b[m.first.toInt()][i] != player) i++
                if (i < rows && b[m.first.toInt()][i] == player) {
                    i = (i - 1)
                    while (i > m.second) {
                        result.add(Pair<Byte, Byte>(m.first, i.toByte()))
                        i--
                    }
                }
            }
        }
        if (m.second - 2 >= 0) {
            if (b[m.first.toInt()][m.second - 1] == nextPlayer()) {
                var i = (m.second - 2)
                while (i >= 0 && b[m.first.toInt()][i] != AbstractRegularGame.NONE && b[m.first.toInt()][i] != player)
                    i--
                if (i >= 0 && b[m.first.toInt()][i] == player) {
                    i = (i + 1)
                    while (i < m.second) {
                        result.add(Pair<Byte, Byte>(m.first, i.toByte()))
                        i++
                    }
                }
            }
        }

        if (m.first + 2 < columns && m.second + 2 < rows) {
            if (b[m.first + 1][m.second + 1] == nextPlayer()) {
                var i = 2.toByte()
                while (m.first + i < columns
                        && m.second + i < rows
                        && b[m.first + i][m.second + i] != AbstractRegularGame.NONE
                        && b[m.first + i][m.second + i] != player)
                    i++

                if (m.first + i < columns
                        && m.second + i < rows
                        && b[m.first + i][m.second + i] == player) {
                    i = (i - 1).toByte()
                    while (i > 0) {
                        result.add(Pair((m.first + i).toByte() , (m.second + i).toByte()))
                        i--
                    }
                }
            }
        }

        if (m.first - 2 >= 0 && m.second - 2 >= 0) {
            if (b[m.first - 1][m.second - 1] == nextPlayer()) {
                var i = 2
                while (m.first - i >= 0
                        && m.second - i >= 0
                        && b[m.first - i][m.second - i] != AbstractRegularGame.NONE
                        && b[m.first - i][m.second - i] != player)
                    i++
                if (m.first - i >= 0
                        && m.second - i >= 0
                        && b[m.first - i][m.second - i] == player) {
                    i = i - 1
                    while (i > 0) {
                        result.add(Pair((m.first - i).toByte(), (m.second - i).toByte()))
                        i--
                    }
                }
            }
        }

        if (m.first + 2 < columns && m.second - 2 >= 0) {
            if (b[m.first + 1][m.second - 1] == nextPlayer()) {
                var i = 2
                while (m.first + i < columns
                        && m.second - i >= 0
                        && b[m.first + i][m.second - i] != AbstractRegularGame.NONE
                        && b[m.first + i][m.second - i] != player)
                    i++

                if (m.first + i < columns
                        && m.second - i >= 0
                        && b[m.first + i][m.second - i] == player) {
                    i = (i - 1)
                    while (i > 0) {
                        result.add(Pair((m.first + i).toByte(), (m.second - i).toByte()))
                        i--
                    }
                }
            }
        }
        if (m.first - 2 >= 0 && m.second + 2 < rows) {
            if (b[m.first - 1][m.second + 1] == nextPlayer()) {
                var i = 2
                while (m.first - i >= 0
                        && m.second + i < rows
                        && b[m.first - i][m.second + i] != AbstractRegularGame.NONE
                        && b[m.first - i][m.second + i] != player)
                    i++
                if (m.first - i >= 0
                        && m.second + i < rows
                        && b[m.first - i][m.second + i] == player) {
                    i = (i - 1)
                    while (i > 0) {
                        result.add(Pair((m.first - i).toByte(), (m.second + i).toByte()))
                        i--
                    }
                }
            }
        }
        return result
    }

    fun mkMove(m: Pair<Byte, Byte>): OthelloMove {
        if (b[m.first.toInt()][m.second.toInt()] == AbstractRegularGame.NONE) {
            val turn = toTurn(m)
            if (!turn.isEmpty())
                return OthelloMove(m, turn)
        }
        return NoMove()
    }

    override fun doMove(m: OthelloMove): Othello {
        val om = m.mv
        val result = copy()
        result.player = nextPlayer()
        if (m is NoMove) return result

        result.b[om.first.toInt()][om.second.toInt()] = player
        for (t in m.turn)
            result.b[t.first.toInt()][t.second.toInt()] = player

        result.movesDone = this.movesDone + 1
        return result
    }

    fun setAtPosition(column: Byte, row: Byte): Othello {
        return doMove(mkMove(Pair(column, row)))
    }

    override fun legalMoves(): List<OthelloMove> {
        val result = mutableListOf<OthelloMove>()
        for (trs in 0 until columns)
            for (pos in 0 until rows) {
                val m = Pair<Byte, Byte>(trs.toByte(), pos.toByte())
                if (b[trs][pos] == AbstractRegularGame.NONE) {
                    val turn = toTurn(m)
                    if (!turn.isEmpty())
                        result.add(OthelloMove(m, turn))
                }
            }
        if (result.isEmpty()) result.add(NoMove())
        return result
    }
}
