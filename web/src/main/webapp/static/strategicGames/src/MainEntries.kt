import kotlin.browser.document
import kotlin.dom.appendText

fun viergewinnt() {
    var board = GameBoard(Vier())

    document.getElementById("main")?.appendChild(board.div)
    document.getElementById("main")?.appendText("Spiele eine kleine Runde »Vier Gewinnt«")
}

fun tictactoe() {
    var board = GameBoard(TicTacToe())

    document.getElementById("main")?.appendChild(board.div)
    document.getElementById("main")?.appendText("Spiele eine kleine Runde TicTacToe")
}


fun othello() {
    var board = GameBoard(Othello())

    document.getElementById("main")?.appendChild(board.div)
    document.getElementById("main")?.appendText("Spiele eine kleine Runde Othello")
}