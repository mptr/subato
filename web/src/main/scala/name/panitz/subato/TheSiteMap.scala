package name.panitz.subato

import net.liftweb.util.HttpHelpers
import net.liftweb.http.provider.HTTPCookie
import java.net.HttpCookie
import net.liftweb.common._
import net.liftweb.http._
import scala.xml.Text
import net.liftweb.util.Helpers._
import net.liftweb.sitemap.Loc._
import net.liftweb.sitemap.Loc
import net.liftweb.sitemap._

import name.panitz.subato.model._

object TheSiteMap extends MapperModelDependencies with SWrapperL10n{

  val menus : List[net.liftweb.sitemap.ConvertableToMenu]
   = Menu(locStr("home")) / "index"  ::
      (List
        (Menu(Loc("Terms",(("Terms" :: Nil), false),locStr("terms")
           )) /*
          If(currentUser.loggedIn _, "You are not logged in.") )) */
          , Menu(Loc("Sls", (("Sls" :: Nil), false), locStr("SubatoLearningSystem")/*,
          If(currentUser.loggedIn _, "You are not logged in.")*/ ))
          /*, Menu(Loc("AdminFlashcardCourses", (("AdminFlashcardCourses" :: Nil), false), locStr("adminFlashcardCourses"),
            If(currentUser.loggedIn _, "You are not logged in.")))*/
          //, Menu(Loc("CardBoxes",(("CardBoxes" :: Nil), false), locStr("cardboxes")))
          , Menu(Loc("Surveys",(("Surveys" :: Nil), false),locStr("surveys")))
          , Menu(Loc("DuelList",(("DuelList" :: Nil), false),locStr("duels"), If(currentUser.loggedIn _, "You are not logged in.")))
          , Menu(new IdLoc(duels, "Duel"))
          , Menu(Loc("SlsStatistics" , (("SlsStatistics" :: Nil), false), "SlsStatistics", Hidden))
          , Menu(Loc("SlsStudio", (("SlsStudio" :: Nil), false), "SlsStudio", Hidden, If(currentUser.isLecturer _, "You are not a registered lecturer.")))
          //, Menu(Loc("AdminFlashcards",(("AdminFlashcards" :: Nil), false),locStr("adminFlashcards"),
          //  If(currentUser.isLecturer _, "You are not registered as lecturer.") ))
          , Menu(Loc("AdminSurveys",(("AdminSurveys" :: Nil), false),locStr("adminSurveys"),
            If(currentUser.isLecturer _, "You are not registered as lecturer.") ))
          , Menu(Loc("CreateCardBox",(("CreateCardBox" :: Nil), false),locStr("createcardbox"),
             Hidden ))
          , Menu(Loc("ListCourses",(("ListCourses" :: Nil), false),locStr("listCourses"),
             If(currentUser.isLecturer _, "You are not registered as lecturer.") ))
          , Menu(Loc("Results", (("Results" :: Nil), false), "Results",Hidden
//            ,If(currentUser.loggedIn _, "You are not logged in.")
              ))
          , Menu(Loc("AbnahmeTestResults", (("AbnahmeTestResults" :: Nil), false), "AbnahmeTestResults",Hidden,
            If(currentUser.loggedIn _, "You are not logged in.")))
          ,  Menu(new IdLoc(terms, "Term"))
          ,  Menu(new IdLoc(testExecutors, "testExecutors"))
          ,  Menu(new IdLocLecturer(cardBoxes, "ListCards"))
          ,  Menu(new IdLocLecturer(courses, "ListExercises"))
          ,  Menu(new IdLocLecturer(courses, "ImportExercise"))
          ,  Menu(new IdLocLecturer(exercises, "UploadFile"))
          ,  Menu(new IdLocLecturer(coursePages, "UploadFileForCourse"))
          ,  Menu(new IdLocLecturer(courses, "ImportExercises"))
          ,  Menu(new IdLocLecturer(courses, "ImportCardBox"))
          ,  Menu(new IdLocLecturer(courses, "ImportSurvey"))
          ,  Menu(new IdLocLecturer(exerciseSheets, "SheetAddEntry"))
          ,  Menu(new IdLocPublic(cardBoxes, "CardBox"))
          ,  Menu(new IdLocPublic(multipleCards, "ShowSingleCard"))
          ,  Menu(new IdLocPublic(surveys, "SurveyResult"))
          ,  Menu(new IdLocPublic(surveys, "SurveyThanks"))
          ,  Menu(new IdLocPublic(surveys, "Survey"))
          ,  Menu(new IdLocPublic(cardBoxes, "CardResult"))
          ,  Menu(new IdLocPublic(flashcardCourses, "SlsCourse"))
          ,  Menu(new IdLocPublic(flashcardCourses, "SlsCardResult"))
          ,  Menu(new IdLocLecturer(cardBoxes, "DeleteCardbox"))
          ,  Menu(new IdLocLecturer(surveys, "DeleteSurvey"))
          ,  Menu(new IdLocLecturer(surveys, "ClearSurvey"))
          ,  Menu(new IdLoc(courses, "ListExerciseSheets"))
          ,  Menu(new IdLocPublic(coursePages, "CoursePage"))
          ,  Menu(new IdLocPublic(groups, "ListGroupMembers"))
          ,  Menu(new IdLocLecturer(coursePages, "EditCoursePage"))
          ,  Menu(new IdLocLecturer(lessons, "EditLesson"))
          ,  Menu(new IdLocPublic(coursePages, "ListCourseMember"))
          ,  Menu(new IdLocPublic(coursePages, "JoinCoursePage"))
          ,  Menu(new CourseLoc)
          ,  Menu(new GroupLoc)
          ,  Menu(new SheetLoc)
          ,  Menu(new CreateSheetLoc)
          ,  Menu(new CreateGroupLoc)
          ,  Menu(new CreateTutorLoc)
          ,  Menu(new ListSLSCourseLoc)
          ,  Menu(new AddSLSCourseLoc)
          ,  Menu(new CreateCoursePageForCourseAndTermLoc)
          ,  Menu(new EditTextResourceLoc)
          ,  Menu(new DeleteTextResourceLoc)
          ,  Menu(new EditTextCourseResourceLoc)
          ,  Menu(new DeleteTextCourseResourceLoc)
          ,  Menu(new TestSampleSolutionLoc)
          ,  Menu(new PlagiarismLoc)
          ,  Menu(new TGZLoc)
          ,  Menu(new AbnahmeTestLoc)
          ,  Menu(new TutorLoc)
          ,  Menu(new RateLessonLoc)
          ,  Menu(new ResultsLoc)
          ,  Menu(new SheetResultsLoc)
          ,  Menu(new CoursePageStudentResultsLoc)
          ,  Menu(new PointsForSheetAndUserLoc)
          ,  Menu(new AttendStudentInCourseLoc)
          ,  Menu(new ExerciseStudentResultsLoc)
          ,  Menu(new StudentSheetResultsLoc)
          ,  Menu(new GrantExerciseSheetForStudentLoc)
          ,  Menu(new IdLocLecturer(exercises, "AddResourceFileForExercise"))
          ,  Menu(new IdLocLecturer(coursePages, "AddResourceFileForCourse"))
          ,  Menu(new IdLocLecturer(exercises, "ListResourceFilesForExercise"))
          ,  Menu(new IdLocLecturer(coursePages, "ListResourceFilesForCourse"))
          ,  Menu(new IdLocLecturer(coursePages, "CreateLessonForCoursePage"))
          ,  Menu(new IdLoc(courses, "ProposeCard"))
          ,  Menu(new ExerciseLoc   ) //new Loc2(exercises, "Exercise"))
          ,  Menu(new IdLoc(exercises, "Solution"))
          ,  Menu(new IdLocPublic(exerciseSheets, "ExerciseSheet"))
          ,  Menu(new IdLoc(duels, "DuelResult"))
          ,  Menu(new IdLoc(duels, "PlayDuel"))
          ,  Menu(Loc("CreateNewDuel",(("CreateNewDuel" :: Nil), false),"CreateNewDuel", List(Hidden,If(currentUser.loggedIn _, "You are not logged in."))))
          /*,  Menu(Loc("FlashcardCourseStudio", (("FlashcardCourseStudio" :: Nil), false), "FlashcardCourseStudio",
            List(Hidden, If(currentUser.loggedIn _, "You are not logged in."))))*/
          ,  Menu(new IdLocLecturer(exerciseSheets, "ListExerciseSheetEntries"))
        )
        ++ mapper.learnsystem.FlashcardCourse.menus
        ++ mapper.Exercise.menus
        ++ mapper.ExerciseSheet.menus
        ++ mapper.ExerciseSheetEntry.menus
        ++ mapper.Group.menus
        ++ mapper.Course.menus
//        ++ mapper.CoursePage.menus
        ++ mapper.Term.menus
        ++ mapper.ResourceFile.menus
        ++ mapper.ResourceFileForCourse.menus
        ++ mapper.IsLecturerIn.menus
        ++ mapper.SLSCourseInCourseTerm.menus
        ++ mapper.IsTutorIn.menus
        ++ mapper.Ontology.menus
        ++ mapper.TestExecutor.menus
        ++ mapper.Lesson.menus
        ++ mapper.Admin.menus
        ++ mapper.Submission.menus
        ++ mapper.MultipleCard.menus
        ++ mapper.Answer.menus
//        ++ mapper.CardBox.menus
//        ++ mapper.CardBoxEntry.menus
        ++ List(Menu(Loc("About", Link(List("static"), true, "/static/help_de.html"), locStr("aboutsubato"))) )
//        ++ List(Menu("Impressum") / "static/impressum.html"  )
        ++ List(Menu(Loc("Impressum", Link(List("static2"), true, "/static/impressum.html"), "Impressum")) )
//        ++ List(Menu(Loc("Clock",(("Clock" :: Nil), false),"Clock")))
        ++ mapper.User.menus
      )

  def siteMap = SiteMap(menus :_* )
}

class IdLocLecturer[ModelType <: IdAndNamed](finder: Finder[ModelType], urlPart: String)
    extends IdLoc[ModelType](finder,urlPart)
    with Logger
    with MapperModelDependencies {

  override def params =
    Hidden ::
     If(currentUser.loggedIn _, RedirectResponse("/user_mgt/login", LocHelpers.redirectCookie)) ::
     If(currentUser.isLecturer _, "Not authorized.") ::
     Nil
}



class IdLoc[ModelType <: IdAndNamed](finder: Finder[ModelType], urlPart: String)
    extends Loc[Box[ModelType]]
    with Logger
    with MapperModelDependencies {

  override def params =
    Hidden ::
     If(currentUser.loggedIn _, RedirectResponse("/user_mgt/login", LocHelpers.redirectCookie)) ::
     Nil

  override def defaultValue = Empty
  override def text = LinkText({
    case Full(c) => Text(c.name)
    case _ => Text("")
  })
  override def name = urlPart
  override def link = new Link(urlPart :: Nil, true)
  override def rewrite: LocRewrite = Full({
    case RewriteRequest(ParsePath(List(urlPart, idstr), _, _, _), _, _) if urlPart == this.urlPart  => idstr match {
      case AsLong(id) => (RewriteResponse( ParsePath(urlPart ::Nil, id+"", false, false) ,Map(("id",id+"")),false), Full(finder.find(id)))
      case _ => (RewriteResponse(urlPart :: Nil), Empty)
        }
    })
}


class IdLocPublic[ModelType <: IdAndNamed](finder: Finder[ModelType], urlPart: String)
    extends Loc[Box[ModelType]]
    with Logger
    with MapperModelDependencies {

  override def params = Hidden :: Nil
  override def defaultValue = Empty
  override def text = LinkText({
    case Full(c) => Text(c.name)
    case _ => Text("")
  })
  override def name = urlPart
  override def link = new Link(urlPart :: Nil, true)
  override def rewrite: LocRewrite = Full({
    case RewriteRequest(ParsePath(List(urlPart, idstr), _, _, _), _, _) if urlPart == this.urlPart  => idstr match {
      case AsLong(id) => (RewriteResponse(urlPart :: Nil), Full(finder.find(id)))
      case _ => (RewriteResponse(urlPart :: Nil), Empty)
        }
    })
}


class Loc2[PrimaryType <: IdAndNamed](urlName:String,finder:Finder[PrimaryType])
    extends Loc[(String, Box[PrimaryType])]
    with MapperModelDependencies {
  
  override def name = urlName
  override def link = new Link((urlName :: Nil), true)
  override def params =
    Hidden ::
    If(currentUser.loggedIn _, RedirectResponse("/user_mgt/login", LocHelpers.redirectCookie)) ::
    Nil
  override def defaultValue = Empty
  override def text = LinkText({
    case (termid, Full(e)) => Text(termid + "/" + e.name)
    case _ => Text("")
  })

  override def rewrite: LocRewrite = Full({
    case RewriteRequest(ParsePath(List(urlName, AsLong(courseId), termid), _, _, _), _, _) if urlName == this.urlName => {
      val snippetParam = Full(termid, finder.find(courseId))
      (RewriteResponse(urlName :: Nil), snippetParam)
    }
  })
}

class Loc2Lecturer[PrimaryType <: IdAndNamed](urlName:String,finder:Finder[PrimaryType])
    extends  Loc2(urlName,finder){
  override def params =
    Hidden ::
    If(currentUser.loggedIn _, RedirectResponse("/user_mgt/login", LocHelpers.redirectCookie)) ::
    If(currentUser.isLecturer _, "Not authorized.") ::
    Nil
}

class Loc2Tutor[PrimaryType <: IdAndNamed](urlName:String,finder:Finder[PrimaryType])
    extends Loc2(urlName,finder){
  override def params =
    Hidden ::
    If(currentUser.loggedIn _, RedirectResponse("/user_mgt/login", LocHelpers.redirectCookie)) ::
    If(currentUser.isTutor _, "Not authorized.") ::
    Nil
}

class Loc2Public[PrimaryType <: IdAndNamed](urlName:String,finder:Finder[PrimaryType])
    extends Loc2(urlName,finder){
  override def params =   Hidden::Nil
}


object LocHelpers {
  def redirectCookie = HTTPCookie(
    "origUri", //name
    Full(S.uri), //value
    Empty, //domain
    Full("/"), //path
    Full(60), //time in seconds
    Empty, //version
    Full(false)) //secure?
}

class EditTextResourceLoc extends Loc2Lecturer[Exercise]("EditTextResource",MapperExercises)
class DeleteTextResourceLoc extends Loc2Lecturer[Exercise]("DeleteTextResource",MapperExercises)
class EditTextCourseResourceLoc extends Loc2Lecturer[CoursePage]("EditTextCourseResource",MapperCoursePages)
class DeleteTextCourseResourceLoc extends Loc2Lecturer[CoursePage]("DeleteTextCourseResource",MapperCoursePages)
class TestSampleSolutionLoc extends Loc2Lecturer[Exercise]("TestSampleSolution",MapperExercises)
class PlagiarismLoc extends Loc2Lecturer[Exercise]("Plagiarism",MapperExercises)
class TGZLoc extends Loc2Lecturer[Exercise]("TGZSubmissions",MapperExercises)

class AbnahmeTestLoc extends Loc2Lecturer[Exercise]("AbnahmeTest",MapperExercises)
class CreateTutorLoc extends Loc2Lecturer[Course]("CreateTutorForCourseAndTerm",MapperCourses)
class CreateGroupLoc extends Loc2Lecturer[Course]("CreateGroupForCourseAndTerm",MapperCourses)
class CreateSheetLoc extends Loc2Lecturer[Course]("CreateSheetForCourseAndTerm",MapperCourses)
class AddSLSCourseLoc extends Loc2Lecturer[Course]("AddSLSCourseForCourseAndTerm",MapperCourses)

class CreateCoursePageForCourseAndTermLoc extends Loc2Lecturer[Course]("CreateCoursePageForCourseAndTerm",MapperCourses)

class SheetLoc extends Loc2Lecturer[Course]("ListSheetsForCourseAndTerm",MapperCourses)
class GroupLoc extends Loc2Lecturer[Course]("ListGroupsForCourseAndTerm",MapperCourses)
class TutorLoc extends Loc2Lecturer[Course]("ListTutorsForCourseAndTerm",MapperCourses)

class RateLessonLoc extends Loc2[Lesson]("RateLesson",MapperLessons)

class ListSLSCourseLoc extends Loc2Lecturer[Course]("ListSLSCourseForCourseAndTerm",MapperCourses)
class CourseLoc extends Loc2Public[Course]("Course",MapperCourses)
class ExerciseLoc extends Loc2Public[Exercise]("Exercise",MapperExercises)

class ResultsLoc extends Loc3Tutor[Exercise]("StudentsByExerciseTerm",MapperExercises)
class SheetResultsLoc extends Loc2Tutor[ExerciseSheet]("StudentsBySheetTerm",MapperExerciseSheets)

class CoursePageStudentResultsLoc extends Loc2[CoursePage]("CoursePageStudentResults",MapperCoursePages)
class AttendStudentInCourseLoc    extends Loc2[CoursePage]("AttendStudentInCourse",MapperCoursePages)

class PointsForSheetAndUserLoc extends Loc2[ExerciseSheet]("PointsForSheetAndUser",MapperExerciseSheets)


class ExerciseStudentResultsLoc extends Loc3[Exercise]("ExerciseResults",MapperExercises)
class StudentSheetResultsLoc extends Loc3[ExerciseSheet]("SheetResults",MapperExerciseSheets)


                                                                        
class GrantExerciseSheetForStudentLoc extends Loc3Tutor[ExerciseSheet]("GrantExerciseSheetForStudent",MapperExerciseSheets)





class Loc3[PrimaryType <: IdAndNamed](urlName:String,finder:Finder[PrimaryType])
    extends Loc[(String, String, Box[PrimaryType])]
    with MapperModelDependencies {
  override def name = urlName
  override def link = new Link((urlName :: Nil), true)
  override def params = Hidden ::
    If(currentUser.loggedIn _, RedirectResponse("/user_mgt/login", LocHelpers.redirectCookie)) ::
    Nil
  override def defaultValue = Empty
  override def text = LinkText({
    case (uid,termid, Full(e)) => Text(uid+"/"+termid + "/" + e.id)
    case _ => Text("")
  })

  override def rewrite: LocRewrite = Full({
    case RewriteRequest(ParsePath(List(urlName, AsLong(exerciseId), termid,uid), _, _, _), _, _) if this.urlName==urlName => {
      val snippetParam = Full(uid,termid, finder.find(exerciseId))
      (RewriteResponse(urlName :: Nil), snippetParam)
    }
  })
}

class Loc3Tutor[PrimaryType <: IdAndNamed](urlName:String,finder:Finder[PrimaryType])
    extends Loc3(urlName,finder){
  override def params =
    Hidden ::
    If(currentUser.loggedIn _, RedirectResponse("/user_mgt/login", LocHelpers.redirectCookie)) ::
    If(currentUser.isTutor _, "Not authorized.") ::
    Nil
}


