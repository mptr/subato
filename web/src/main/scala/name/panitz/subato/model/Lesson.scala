package name.panitz.subato.model

import net.liftweb.mapper.By
import net.liftweb.mapper.OrderBy
import net.liftweb.mapper.Ascending
import net.liftweb.common.Box
import mapper.{Lesson => MLesson}
import java.time.LocalTime
import java.time.LocalDateTime
import scala.xml._

trait Lesson extends IdAndNamed {
  def coursePage:Box[CoursePage]
  def coursePageId:Long
  def date:LocalDateTime
  def content:Node
  def star1:Int
  def star1_=(star1:Int)
  def star2:Int
  def star2_=(star2:Int)
  def star3:Int
  def star3_=(star3:Int)
  def star4:Int
  def star4_=(star4:Int)
  def star5:Int
  def star5_=(star5:Int)
  def stars:Int
  def addStars(stars:Int):Float
  def evaluations:Int
  def averageEvaluation:Float
  def hasRated(cu:CurrentUser):Boolean
  def evaluationAsHTML:Node
}

trait Lessons extends Finder[Lesson] {
  def findByCoursePage(coursePageId:Long):List[Lesson]
  def createAndSave(coursePage:CoursePage):Lesson
}


object MapperLessons extends Lessons with MapperFinder[Lesson, MLesson] {
  override def createAndSave(coursePage:CoursePage):Lesson = {
    val mg = new MLesson().coursePage(coursePage.id)
    mg.save()
    val res = new MapperLesson(mg)
    res
  }
  override def wrap(in:MLesson):Lesson = new MapperLesson(in)
  override def metaMapper = MLesson

  override def findByCoursePage(coursePageId:Long)
    = MLesson.findAll(By(MLesson.coursePage, coursePageId),OrderBy(MLesson.date,Ascending)).map(wrap(_))
}



class MapperLesson(private val ml:MLesson) extends Lesson with MapperModelDependencies{
  def id = ml.id.get
  def name = "Lecture "+coursePage.map(_.name).openOr("")+" "+date
  def coursePage:Box[CoursePage]=ml.coursePage.obj.map(new MapperCoursePage(_))
  def coursePageId:Long= ml.coursePage.get
  def date:LocalDateTime={
    val t = ml.date.get
    if (t == null) return null
    val input = t.toString.replace( " " , "T" );
    val ldt = LocalDateTime.parse( input );
    ldt;
  }
  def content:Node=ml.content.asHtml
  def star1:Int=ml.star1.get
  def star1_=(star1:Int) {ml.star1.set(star1)}
  def star2:Int=ml.star2.get
  def star2_=(star2:Int) {ml.star2.set(star2)}
  def star3:Int=ml.star3.get
  def star3_=(star3:Int) {ml.star3.set(star3)}
  def star4:Int=ml.star4.get
  def star4_=(star4:Int) {ml.star4.set(star4)}
  def star5:Int=ml.star5.get
  def star5_=(star5:Int) {ml.star5.set(star5)}
  def stars:Int=star1+2*star2+3*star3+4*star4+5*star5
  def addStars(stars:Int):Float = {
    stars match {
      case 1 => {ml.star1.set(star1+1)}
      case 2 => {ml.star2.set(star2+1)}
      case 3 => {ml.star3.set(star3+1)}
      case 4 => {ml.star4.set(star4+1)}
      case 5 => {ml.star5.set(star5+1)}
    }
    ml.save
    averageEvaluation
  }
  def evaluations:Int=star1+star2+star3+star4+star5
  def averageEvaluation:Float=stars*1f/evaluations

  def hasRated(cu:CurrentUser):Boolean=cu.loggedIn && !MapperHasRatedLessons.findBy(id,cu.uid.openOr("")).isEmpty

  def evaluationAsHTML={
    val theStars = averageEvaluation
    val fullstars = theStars.round
    <div class="evaluation">
      {for (i<-1 to fullstars)yield <span class="teacupGold">☕&#xFE0E;</span>}
      {for (i<-(fullstars+1) to 5)yield <span class="teacupGrey">☕&#xFE0E;</span>}
      (avg: {"%1.1f".format(theStars)},total#: {evaluations})
    </div>
  }
}

