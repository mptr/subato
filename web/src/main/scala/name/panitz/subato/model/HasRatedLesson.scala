package name.panitz.subato.model

import net.liftweb.mapper.By
import net.liftweb.common.Box
import mapper.{HasRatedLesson => MHasRatedLesson}
import java.time.LocalTime
import java.time.LocalDateTime
import scala.xml._

trait HasRatedLesson extends Id {

}

trait HasRatedLessons extends Finder[HasRatedLesson] {
  def findBy(lessonId:Long,uid:String):List[HasRatedLesson]
  def createAndSave(lesson:Lesson,uid:String):HasRatedLesson
}


object MapperHasRatedLessons extends HasRatedLessons with MapperFinder[HasRatedLesson, MHasRatedLesson] {
  override def wrap(in:MHasRatedLesson):HasRatedLesson = new MapperHasRatedLesson(in)
  override def metaMapper = MHasRatedLesson

  override def createAndSave(lesson:Lesson,uid:String):HasRatedLesson = {
    val mg = new MHasRatedLesson().lesson(lesson.id).uid(uid)
    mg.save()
    val res = new MapperHasRatedLesson(mg)
    res
  }
  override def findBy(lessonId:Long,uid:String)
    = MHasRatedLesson.findAll(By(MHasRatedLesson.lesson, lessonId),By(MHasRatedLesson.uid,uid)).map(new MapperHasRatedLesson(_))
}



class MapperHasRatedLesson(private val ml:MHasRatedLesson) extends HasRatedLesson {
  def id = ml.id.get
}
