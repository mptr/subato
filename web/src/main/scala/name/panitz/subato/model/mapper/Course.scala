package name.panitz.subato.model.mapper

import net.liftweb.mapper.MappedPoliteString
import net.liftweb.http.UnauthorizedResponse
import net.liftweb.mapper.By
import net.liftweb.http.RedirectResponse
import net.liftweb.sitemap.Loc.If
import net.liftweb.sitemap.Loc
import net.liftweb.sitemap.Menu
import net.liftweb.common.Full
import net.liftweb.mapper.MappedTextarea
import net.liftweb.mapper.MappedString
import net.liftweb.mapper.LongKeyedMetaMapper
import net.liftweb.mapper.CRUDify
import net.liftweb.mapper.LongKeyedMapper
import net.liftweb.mapper.IdPK

class Course
    extends LongKeyedMapper[Course]
    with IdPK {

  def getSingleton = Course
	
  object name extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }
  object description extends MappedWysiwygEditor(this, ()=>"description")

  def exercises() = Exercise.findAll(By(Exercise.course, id.get))
  def exerciseSheets(termId:Long)
    = ExerciseSheet.findAll(By(ExerciseSheet.course, id.get),By(ExerciseSheet.term, termId))

  def exerciseSheets
    = ExerciseSheet.findAll(By(ExerciseSheet.course, id.get))

//  def resourceFiles = ResourceFileForCourse.findAll(By(ResourceFileForCourse.course, id.get))

}

object Course extends Course
    with LongKeyedMetaMapper[Course]
    with AdminOnlyCRUDify[Long, Course] {
	
  private def deleteIsLecturerIn(c:Course) {
    IsLecturerIn.bulkDelete_!!(By(IsLecturerIn.course, c))
  }
  private def unsetCourseInExercises(c:Course) {
    for (e <- c.exercises) {
      e.course(-1).save
    }
  }
  override def beforeDelete = deleteIsLecturerIn _ :: unsetCourseInExercises _ :: super.beforeDelete
}
