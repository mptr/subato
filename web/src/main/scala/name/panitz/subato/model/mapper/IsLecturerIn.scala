package name.panitz.subato.model.mapper

import net.liftweb.mapper.MappedPoliteString
import net.liftweb.mapper.MappedString
import net.liftweb.http.UnauthorizedResponse
import net.liftweb.sitemap.Loc.If
import net.liftweb.common.Full
import net.liftweb.mapper.{By, IdPK, LongKeyedMapper, LongKeyedMetaMapper, MappedLongForeignKey}
import net.liftweb.sitemap.Loc
import scala.xml.Text

class IsLecturerIn
    extends LongKeyedMapper[IsLecturerIn]
    with IdPK {
  def getSingleton = IsLecturerIn

  object uid extends MappedPoliteString(this, 100) {
    override def dbIndexed_? = true
  }
  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def validSelectValues = Full(Course.findAll.map(x => (x.id.get, x.name.get)))
    override def asHtml() = {
      Text(Course.find(By(Course.id, this.get)) map {_.name.get} openOr "")
    }
  }
  object term extends MappedLongForeignKey(this, Term) {
    override def dbIndexed_? = true
    override def validSelectValues = Full(Term.findAll.map(x => (x.id.get, x.addendum.get+" "+x.year.get)))
    override def asHtml() = {
      Text(Term.find(By(Term.id, this.get)) map {(t) => t.addendum.get +" "+t.year.get  } openOr "")
    }
  }



}

object IsLecturerIn
    extends IsLecturerIn
    with LongKeyedMetaMapper[IsLecturerIn]
    with AdminOnlyCRUDify[Long, IsLecturerIn] {
  import xml._
  

}
