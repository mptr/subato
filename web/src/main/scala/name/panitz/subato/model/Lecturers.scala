package name.panitz.subato.model

import net.liftweb.mapper.By
import name.panitz.subato.model.mapper.IsLecturerIn
import net.liftweb.common.Full

trait Lecturers {
  def contains(uid:String, course:Course):Boolean
  def contains(uid:String, courseId:Long):Boolean
  def contains(uid:String, courseId:Long, termId:Long):Boolean
  def contains(uid:String):Boolean
}

private[model] object MapperLecturers extends Lecturers {
  override def contains(uid:String, courseId:Long) = {
    IsLecturerIn.find(By(IsLecturerIn.uid, uid), By(IsLecturerIn.course, courseId)) match {
      case Full(_) => true
      case _ => false
    }
  }
  override def contains(uid:String, courseId:Long, termId:Long) = 
    IsLecturerIn.find(By(IsLecturerIn.uid, uid), By(IsLecturerIn.course, courseId), By(IsLecturerIn.term, termId)) match {
      case Full(_) => true
      case _ => false
    }
  

  override def contains(uid:String, course:Course) = contains(uid, course.id)
  override def contains(uid:String) = IsLecturerIn.count(By(IsLecturerIn.uid, uid)) > 0
}
