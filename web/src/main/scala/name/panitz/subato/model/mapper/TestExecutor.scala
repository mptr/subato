package name.panitz.subato.model.mapper

import net.liftweb.mapper.{IdPK, LongKeyedMapper, LongKeyedMetaMapper, MappedPoliteString}

class TestExecutor extends LongKeyedMapper[TestExecutor]
  with IdPK {

  def getSingleton = TestExecutor

  object name extends MappedPoliteString(this, 100) {}
  object image extends MappedPoliteString(this, 300) {}
  object language extends MappedPoliteString(this, 100) {}

}

object TestExecutor extends TestExecutor
  with LongKeyedMetaMapper[TestExecutor]
  with AdminOnlyCRUDify[Long, TestExecutor] {

}
