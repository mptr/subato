package name.panitz.subato.model.mapper

import scala.xml._
import net.liftweb.common._
import net.liftweb.mapper._
import net.liftweb.util.Helpers._

import java.text.DateFormat
import java.util.Date

import net.liftweb.common.{Full, Box}
import net.liftweb.http.{ SHtml }

import java.time.LocalTime

import name.panitz.subato.model.Weekday

private[model] class SLSCourseInCourseTerm extends LongKeyedMapper[SLSCourseInCourseTerm] with IdPK {
  def getSingleton = SLSCourseInCourseTerm

  object course extends MappedLongForeignKey(this, Course) {
    override def dbIndexed_? = true
    override def validSelectValues = Full(Course.findAll.map(x => (x.id.get, x.name.get)))
    override def asHtml() = {
      Text(Course.find(By(Course.id, this.get)) map {_.name.get} openOr "")
    }
  }
  object term extends MappedLongForeignKey(this, Term) {
    override def dbIndexed_? = true
    override def validSelectValues = {
     Full(for (term <- Term.findAll())yield {
          (term.id.get, (term.addendum+" "+term.year))
     })
    }
    override def asHtml() = {
      Text(Term.find(By(Term.id, this.get)) map {(t) => t.addendum.get+" "+t.year.get} openOr "")
    }
  }

  object flashcardCourse extends MappedLongForeignKey(this, learnsystem.FlashcardCourse) {
    override def dbIndexed_? = true
    override def validSelectValues = Full(learnsystem.FlashcardCourse.findAll.map(x => (x.id.get, x.title.get)))
    override def asHtml() = {
      Text(learnsystem.FlashcardCourse.find(By(learnsystem.FlashcardCourse.id, this.get)) map {_.title.get} openOr "")
    }
  }

  def courseName = Course.find(By(Course.id, course.get)) map {_.name.get} openOr ""
  def flashcardCourseName = learnsystem.FlashcardCourse.find(By(learnsystem.FlashcardCourse.id, flashcardCourse.get)) map {_.title.get} openOr ""


  
}

import net.liftweb.sitemap.Loc.Hidden
import net.liftweb.common.Empty


object SLSCourseInCourseTerm extends SLSCourseInCourseTerm with LongKeyedMetaMapper[SLSCourseInCourseTerm]
    with LecturerOnlyCRUDify[Long, SLSCourseInCourseTerm] {
  override def showAllMenuLoc = Empty
  override def createMenuLocParams = Hidden :: super.createMenuLocParams

}
