package name.panitz.subato.model.mapper

import net.liftweb.mapper._

class StatisticData extends LongKeyedMapper[StatisticData] with IdPK {

  override def getSingleton: StatisticData.type = StatisticData

  def this(mc: Long) {
    this()
    this.flashcardId.set(mc)
    this.goodAnswers(0)
    this.wrongAnswers(0)
    this.almostCorrectAnswers(0)
  }

  def incGoodAnswer(neededSeconds: Long) {
    this actualizeAverageSolutionTime neededSeconds
    this.goodAnswers.set(this.goodAnswers + 1)
    this.save()
  }

  def incWrongAnswer(neededSeconds: Long) {
    this actualizeAverageSolutionTime neededSeconds
    this.wrongAnswers.set(this.wrongAnswers + 1)
    this.save()
  }

  def incAlmostCorrectAnswer(neededSeconds: Long) {
    this actualizeAverageSolutionTime neededSeconds
    this.almostCorrectAnswers.set(this.almostCorrectAnswers + 1)
    this.save()
  }

  private def actualizeAverageSolutionTime(seconds: Long): Unit = {
    val playedTimes = this.wrongAnswers.get + this.goodAnswers.get + this.almostCorrectAnswers.get
    val newValue = Calculator.average(this.averageSolutionTime.get, seconds.toDouble, playedTimes)
    this.averageSolutionTime.set(newValue)
  }

  object wrongAnswers extends MappedInt(this) {
    override def defaultValue = 0
  }

  object goodAnswers extends MappedInt(this) {
    override def defaultValue = 0
  }

  object almostCorrectAnswers extends MappedInt(this) {
    override def defaultValue = 0
  }

  object averageSolutionTime extends MappedDouble(this) {
    override def defaultValue = 0
  }

  object flashcardId extends MappedLongForeignKey(this, MultipleCard) {
    override def dbIndexed_? = true
  }

}

object StatisticData extends StatisticData with LongKeyedMetaMapper[StatisticData] {}

object Calculator {
  def average(oldAverageValue: Double, valueOfNewElement: Double, elementNumberWithoutNewElement: Long): Double = {
    val elemNum = elementNumberWithoutNewElement.toDouble
     ((elemNum/(elemNum + 1)) * oldAverageValue) + (valueOfNewElement/(elemNum + 1))
  }
}
