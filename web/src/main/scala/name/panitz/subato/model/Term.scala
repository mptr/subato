package name.panitz.subato.model
import name.panitz.subato.model.mapper.{ Term => MT }
import name.panitz.subato.model.mapper.{ Course => CS }


import net.liftweb.common.Box

trait Terms extends Finder[Term] {
  def createAndSave(name: String): Term
}

import scala.xml.Node

trait Term extends IdAndNamed {
  def name: String
  def year: Int
  def addendum: String
  def courses: List[Course]
}

object MapperTerms extends Terms {
  override def find(id: Long) = MT.find(id).map(new MapperTerm(_))
  override def findAll = MT.findAll.map(new MapperTerm(_))
  override def createAndSave(addendum:String) = {
    val mc = new MT().addendum(addendum)
    mc.save()
    new MapperTerm(mc)
  }
}

private[model] class MapperTerm(private val c: mapper.Term) extends Term {
  override def id = c.id.get
  override def year = c.year.get
  override def addendum = c.addendum.get
  override def name = addendum+" "+year
  override def courses: List[Course]={
    CS.findAll.map(new MapperCourse(_))
  }
  override def toString=name
}
