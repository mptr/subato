package name.panitz.subato.model.mapper

import net.liftweb.mapper.KeyedMetaMapper
import net.liftweb.mapper.KeyedMapper
import net.liftweb.sitemap.Loc
import net.liftweb.sitemap.Menu
import net.liftweb.common.Full
import net.liftweb.http.RedirectResponse
import net.liftweb.sitemap.Loc.If
import net.liftweb.mapper.CRUDify
import net.liftweb.mapper.StartAt
import net.liftweb.mapper.MaxRows

private[mapper] trait AccessControlCRUDify[KeyType, CrudType <: KeyedMapper[KeyType, CrudType]]
    extends CRUDify[KeyType, CrudType] {

  this: CrudType with KeyedMetaMapper[KeyType, CrudType] =>

  val accessTest:If

  override def showAllMenuLocParams = accessTest :: super.showAllMenuLocParams
  override def createMenuLocParams = accessTest :: super.createMenuLocParams
  override def editMenuLocParams = accessTest :: super.editMenuLocParams
  override def deleteMenuLocParams = accessTest :: super.deleteMenuLocParams
  override def viewMenuLocParams = accessTest :: super.deleteMenuLocParams
  import net.liftweb.util.Helpers._
  import scala.xml._
  //somehow it did not work with the inherited version
  override def doCrudAllRowItem(c:CrudType ): (NodeSeq)=>NodeSeq = {
    "*" #> {
      for {
        pointer <- fieldsForList
        field <- computeFieldFromPointer(c, pointer).toList
      } yield {
         // ".row-item *" #>
        <td class="row-item">{field.asHtml}</td>
      }
    }
  }

  //somewhow broken???
  override protected def crudAllNext(first: Long, list: List[TheCrudType]): (NodeSeq)=>NodeSeq = {
/*    if (first+rowsPerPage.toLong >=  list.length) {
      (in: NodeSeq) => NodeSeq.Empty
    } else */ {
      "^ <*>" #>
        <a href={listPathString+"?first="+(first + rowsPerPage.toLong)}></a>
    }
  }

}

private[mapper] trait AdminOnlyCRUDify[KeyType, CrudType <: KeyedMapper[KeyType, CrudType]]
    extends AccessControlCRUDify[KeyType, CrudType] {
  this: CrudType with KeyedMetaMapper[KeyType, CrudType] =>
	
  val accessTest = If(User.isAdmin _, () => RedirectResponse("/user_mgt/login"))
}


private[mapper] trait LecturerOnlyCRUDify[KeyType, CrudType <: KeyedMapper[KeyType, CrudType]]
    extends AccessControlCRUDify[KeyType, CrudType] {
  this: CrudType with KeyedMetaMapper[KeyType, CrudType] =>
	
  val accessTest = If(User.isLecturer _, () => RedirectResponse("/user_mgt/login"))
}
private[mapper] trait TutorOnlyCRUDify[KeyType, CrudType <: KeyedMapper[KeyType, CrudType]]
    extends AccessControlCRUDify[KeyType, CrudType] {
  this: CrudType with KeyedMetaMapper[KeyType, CrudType] =>
	
  val accessTest = If(User.isTutor _, () => RedirectResponse("/user_mgt/login"))
}
