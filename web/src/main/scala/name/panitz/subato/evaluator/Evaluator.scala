package name.panitz.subato.evaluator
import net.liftweb.common.Logger
import name.panitz.subato.model.{Exercise, MapperModelDependencies, MapperSubmissions, ModelDependencies, Submission, UnitTestCaseResult}
import name.panitz.subato.ConfigDependency
import name.panitz.subato.FileUtil
import java.io.{BufferedReader, File, FileWriter, InputStream, InputStreamReader}

import net.liftweb.http.FileParamHolder

import scala.xml.XML

abstract class Evaluator {
  this:ModelDependencies
      with Logger
      with ConfigDependency =>

  var graded :Boolean=false
  var time : Long=0
  var solutionDir: String=""
  var path:String=""
  var exDir:String=""
  var files:Array[String]=null

  def testSolution(code:String, userDir:String, exercise:Exercise, courseId:Long, termId:Long, file: FileParamHolder, storeResult:Boolean, isApprovalCheck:Boolean):Submission

  def testSolution(code:String, userDir:String, exercise:Exercise, courseId:Long, termId:Long,file: FileParamHolder):Submission
   =  testSolution(code, userDir, exercise, courseId, termId,file,true, false)

  def init(code:String, userDir:String, exercise:Exercise, courseId:Long, termId:Long){
    val cuid = currentUser.uid.openOr("lambdaUser")
    val allready = submissions.count(cuid, exercise,courseId,termId)
    graded = !exercise.solutionIsPublic && exercise.isGraded  && allready < exercise.attempts

  }

  def extractTestsJar(jarFile:String, exDir:String) = {
    //TODO read paths from config
    //simpleRun(List("ant", "-f", "/var/opt/subato/build.xml", "-Dex.base=" + exDir, "-Dtests.jar=" + jarFile,  "extract-tests-jar"),exDir)
  }

}

  

