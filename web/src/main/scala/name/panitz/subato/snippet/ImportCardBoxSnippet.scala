package name.panitz.subato.snippet


import java.io.{ByteArrayInputStream, File}

import name.panitz.subato.model.{Card, _}
import name.panitz.subato.model.mapper.{CardBoxEntry, TopicsSplitter}
import name.panitz.subato._
import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{FileParamHolder, S, SHtml}
import javax.xml.XMLConstants
import javax.xml.transform.stream
import javax.xml.validation._
import net.liftweb.mapper.By

import scala.xml.Node

class ImportCardBoxSnippet(val c:Box[Course])
    extends AbstractImportCardBoxSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val course = c
}

trait CardCreator extends MapperModelDependencies {
  type Updated = Boolean
  def byNeed(xml: Node, course: Box[Course]): (Card, Updated)
}

object CardCreation extends CardCreator {

  def newCardFrom(xml: Node, course: Box[Course]): Card = {
    val card = createNonSavedCard(xml, course)
    card.save()
    statistics.createAndSave(card.id, 0, 0, 0)
    for ((ans,i) <- (xml \ "answers" \ "answer").zipWithIndex){
      val a = answers.create(card.id, i ,ans.flatMap(_.child)+"", (ans \\ "@correct").text=="true")
      a.save()
    }
    card
  }

  def updateCard(oldCard: Card, xml: Node, course: Box[Course]): Card = {
    val card = createNonSavedCard(xml, course)
    oldCard.deleteAnswers()
    oldCard.update(card.id, card.cardType, card.exerciseText, card.explanation+"", card.name, card.bloomsLevel.toString, card.topics)
    for ((ans, i) <- (xml \ "answers" \ "answer").zipWithIndex) {
      val a = answers.create(oldCard.id, i, ans.flatMap(_.child)+"",(ans \\ "@correct").text=="true")
      a.save()
    }
    oldCard
  }

  override def byNeed(xml: Node, course: Box[Course]): (Card, Updated) = {
    def maybeCard(uid: String): Box[Card] = {
      try {
        val found = multipleCards find uid.toLong
        val sec: String = (xml \ "@sec").text
        if(SecureExportImport.verify(sec, uid, found.openOrThrowException("").salt)) {
          found
        }  else Empty
      } catch {
        case _: Exception => Empty
      }
    }

    val oldCard = maybeCard((xml \ "@uid").text)
    if (oldCard.isDefined) (updateCard(oldCard.openOrThrowException(""), xml, course), true) else (newCardFrom(xml, course), false)
  }

  def extractTopics(xml: Node): Seq[String] = {
    def extractTopic(subXml: Node): String = subXml.text.trim
    for {
      x <- xml \ "topic"
      topic = extractTopic(x)
    } yield topic
  }

  def createNonSavedCard(xml: Node, course: Box[Course]): Card = {
    val cardType =  (xml \ "@type").text
    val bloomsLevel =  (xml \ "@bloomsLevel").text
    val topicNodes = xml \ "topics"
    val courseId = course.openOrThrowException("").id
    val topics = if(topicNodes.isEmpty) Seq.empty else extractTopics(topicNodes.head).filter(!_.isEmpty)
    val result = multipleCards.create(courseId
      , cardType
      , (xml \ "question").flatMap(_.child)
      , (xml \ "explanation").flatMap(_.child)+""
      , (xml \ "name").flatMap(_.child)+""
      , bloomsLevel
      , topics
    )
    if ((result.cardType == "Programming") && (xml \ "exercise").nonEmpty){
      val ex = ImportExercise.createExerciseFromXML((xml \ "exercise").head, courseId)
      result.exerciseId = ex.id
    }
    result
  }
}

trait AbstractImportCardBoxSnippet extends  XmlConfigDependency{
  this:ModelDependencies
      with L10n
      =>

  protected val params:Params
  protected val course:Box[Course]

  def importCardBox={
    var fileHolder : FileParamHolder = null
    def exerciseSubmitted = {
      fileHolder match {
      // An empty upload gets reported with a null mime type,
      // so we need to handle this special case
        case FileParamHolder(_, "text/xml", _, data) => {
          isCardBoxConformToSchema(data) match {
            case (true, _) => {
              val xml =  scala.xml.XML.load(fileHolder.fileStream)
              val Full(c) = course
              val Full(uid) = currentUser.uid
              val Tuple2(box, boxUpdated) = createNewCardBoxOrUpdateFrom(xml, c, uid)
              if (boxUpdated) deleteDeprecatedCards(extractValidCardIds(xml), box)
              for (exXml <- xml \ "cards" \ "card") {
                val Tuple2(card, cardUpdated) = if (boxUpdated) CardCreation.byNeed(exXml, course) else (CardCreation.newCardFrom(exXml, course), false)
                if (!cardUpdated) new CardBoxEntry(box.id, card.id).save
              }
              S.redirectTo("/AdminFlashcards")
            }
            case (false, msg) => {
              S.error(s"Invalid content: $msg")
              false
            }
          }
        }
        case _ => {
          S.error("Invalid receipt attachment")
          false
        }
       }
    }

    course match {
      case Full(c) => {
        "#course" #> c.name &
        "#file *" #> SHtml.fileUpload(fileHolder = _) &
        "#submit *" #> SHtml.submit(locStr("importFlashcardBox"), exerciseSubmitted _) 
      }
      case _ => loc("noCourseSpecified")
    }
  }

  type Updated = Boolean
  def createNewCardBoxOrUpdateFrom(xml: Node, course: Course, uid: String): (CardBox, Updated) = {

    def maybeCardBox(id: String): Box[CardBox] = {
      try {
        val found = cardBoxes find id.toLong
        val opened = found.openOrThrowException("")
        val sec: String = (xml \ "@sec").text
        if ((opened.uid == uid) && SecureExportImport.verify(sec, id, opened.salt)) {
          found
        } else Empty
      } catch {
        case _: Exception => Empty
      }
    }

    val boxName = (xml \ "boxName").text
    val boxDescription = (xml \ "description").flatMap(_.child) + ""
    val maybeBox = maybeCardBox((xml \ "@uid").text)

    if(((xml \ "@update").text == "true") && (!maybeBox.isEmpty)) {
      val oldBox = maybeBox.openOrThrowException("")
      oldBox.update(boxName, boxDescription)
      (oldBox, true)
    } else {
      val box = cardBoxes.create(course.id, uid, boxName, boxDescription)
      box.save()
      (box, false)
    }
  }

  def extractValidCardIds(xml: Node): Seq[Long] = {
    var result = Seq.empty[Long]
    for(cardNode <- xml\"cards"\"card") {
      try {
        result = (cardNode \ "@uid").text.toLong +: result
      } catch {
        case _: Exception =>
      }
    }
    result
  }

  def extractTopics(xml: Node): Seq[String] = {
    def extractTopic(subXml: Node): String = subXml.text
    for {
      x <- xml \ "topic"
      topic = extractTopic(x)
    } yield topic
  }

  def deleteDeprecatedCards(actualCardIds: Seq[Long], box: CardBox): Unit = {
    box.cards filter {x => !actualCardIds.contains(x.id)} foreach(_.delete)
  }

  def isCardBoxConformToSchema(byteArray: Array[Byte]): (Boolean, String) = {
    /*val source = new stream.StreamSource(new ByteArrayInputStream(byteArray))
    try {
      val schema = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(new File("./webapps/subato/cardBox.xsd"))
      val validator = schema.newValidator()
      validator validate source
      (true, "")
    } catch {
      case exc: Exception => (false, s"${exc.getMessage}")
     }*/
    (true, "")
  }

}
