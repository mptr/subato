/* This is a first experimental Hack. Needs cleaning and refactoring. */

package name.panitz.subato.snippet

import java.io.{BufferedOutputStream, FileOutputStream}

import io.grpc.netty.{NegotiationType, NettyChannelBuilder}
import io.grpc.panitz.name.EvaluatorService.service.{EvaluatorServiceGrpc, File, PlagCheckRequest}
import java.io

import scala.concurrent.Future
import scala.concurrent._
import ExecutionContext.Implicits.global
import name.panitz.subato.model._
import name.panitz.subato._
import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{SHtml, StatefulSnippet}

import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class PlagiarismSnippet(val c: Box[Exercise])
  extends AbstractPlagiarismSnippet
    with SWrapperL10n
    with XmlConfigDependency
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  def this(e: (String, Box[Exercise])) = {
    this(e._2)
    this.sheetId = e._1
  }

  override protected var sheetId: String = null
  override protected val params = ParamsImpl
  override val exercise = c
}

trait AbstractPlagiarismSnippet extends name.panitz.subato.XmlConfigDependency
  with Logger {
  this: ModelDependencies
    with L10n =>

  protected val params: Params
  protected val exercise: Box[Exercise]
  protected var sheetId: String

  protected def config: Config


  def Generate = {
    (exercise) match {
      case Full(ex) => {
        val sheetbox = exerciseSheets.find(sheetId.toLong)
        var courseId = ex.course.id
        val termId = sheetbox.openOrThrowException("no sheet specified").term.id
        if (currentUser.isTutorIn(courseId.toLong, termId)) {

          val users1: List[String] = submissions.find(ex, courseId.toLong, termId).map((sub) => sub.uid)
          //alle die die Aufgabe dieses Semester für diesen Kurs abgegeben haben
          val users = users1.distinct

          //und für die brauchen wir doch alle Abgaben?
          val usersAndGroups = users.map(u =>
            (u, userInGroups.findByUid(u, courseId.toLong, termId).map(_.name).openOr(""))
          )

          val lang = if (ex.language == Language.C) "c" else "java19"

          val files = usersAndGroups.map(uag => {
            val (u, group) = uag
            val testResult = submissions.findBest(u, ex.course.id, ex.id, termId.toLong).openOrThrowException("Submission is null")
            new File(u, "test.java", testResult.solution)
          })

          val req = new PlagCheckRequest(files, lang)


          val channel = NettyChannelBuilder.forAddress(sys.env("TEST_EXECUTOR_URL"), sys.env("TEST_EXECUTOR_PORT").toInt).negotiationType(NegotiationType.PLAINTEXT).build()
          val stub = EvaluatorServiceGrpc.blockingStub(channel)

          val result = stub.runPlagCheck(req)
          val jplagDir = config.exBaseDir + ex.id +"/sheet"+sheetId+ "/"

          val dir = new java.io.File(jplagDir)
          if (!dir.exists) dir.mkdirs

          val bos = new BufferedOutputStream(new FileOutputStream(jplagDir + "jplag_result.tar"))
          bos.write(result.result.toByteArray)
          bos.close()

          "#jplugOutput *" #> <pre>
            <code class="language-none">{result.output}</code>
          </pre> &
            "#downloadResult *" #> (<a href={"/serveJPlagResult/" + ex.id + "/" + sheetId}>
              {locStr("downloadplagiarism")}
            </a>)
        } else {
          "*" #> <span>
            {loc("your not allowed to start test for plagiarism")}
          </span>
        }
      }
      case _ => "*" #> <span>
        {loc("noExerciseSpecified")}
      </span>
    }

  }
}



import scala.concurrent.{Future, Await, ExecutionContext}
import scala.concurrent.duration.Duration
import scala.xml.NodeSeq

import net.liftweb.actor.LAFuture
import net.liftweb.http.S
import net.liftweb.util._
import Helpers._

object FutureBinds {
  private def futureTransform[FutureType, T](innerTransform: CanBind[T], resolveFuture: (FutureType) => T): CanBind[FutureType] = new CanBind[FutureType] {
    def apply(future: => FutureType)(ns: NodeSeq): Seq[NodeSeq] = {
      val concreteFuture = future
      val snippetId = s"lazySnippet${Helpers.nextNum}"

      S.mapSnippet(snippetId, { ns: NodeSeq =>
        innerTransform(resolveFuture(concreteFuture))(ns).flatten
      })

      <lift:lazy-load>
        {("^ [data-lift]" #> snippetId) apply ns}
      </lift:lazy-load>
    }
  }

  implicit def futureTransform[T](implicit innerTransform: CanBind[T], executionContext: ExecutionContext): CanBind[Future[T]] = {
    futureTransform[Future[T], T](innerTransform, (future) => Await.result(future, Duration.Inf))
  }

  implicit def lafutureTransform[T](implicit innerTransform: CanBind[T]): CanBind[LAFuture[T]] = {
    futureTransform[LAFuture[T], T](innerTransform, (future) => future.get)
  }
}
