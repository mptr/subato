/* This is a first experimental Hack. Needs cleaning and refactoring. */

package name.panitz.subato.snippet
import scala.concurrent.Future
import scala.concurrent._
import ExecutionContext.Implicits.global
import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class TGZSubmissionsSnippet(val c:Box[ Exercise ])
    extends AbstractTGZSubmissionsSnippet
    with SWrapperL10n
    with XmlConfigDependency 
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  def this(e:(String,Box[Exercise])) = {
    this(e._2)
    this.sheetId = e._1
  }

  override protected var sheetId:String = null
  override protected val params = ParamsImpl
  override val exercise = c
}

trait AbstractTGZSubmissionsSnippet extends  name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
  with L10n  =>

  protected val params:Params
  protected val exercise:Box[ Exercise]
  protected var sheetId:String

  protected def config:Config
	

  def Generate = {
    (exercise) match {
      case Full(ex) => {
        val userDir =
          if (currentUser.loggedIn){
              config.usersBaseDir +
                "/" + currentUser.uid.openOrThrowException("no current user found") +
            "/" + ex.id + "/"

          }else "/dev/null"

        val sheetbox = exerciseSheets.find(sheetId.toLong)
        var courseId = ex.course.id
        val termId   = sheetbox.openOrThrowException("no sheet specified").term.id
        if (currentUser.isTutorIn(courseId.toLong,termId)) {          
          import net.liftweb.mapper._
          val users1:List[String] = submissions.find(ex,courseId.toLong,termId).map((sub)=>sub.uid)
          //alle die die Aufgabe dieses Semester für diesen Kurs abgegeben haben
          val users = users1.distinct

          
          val submission =users.map( u => {
            val testResults = submissions.find(u,ex.id)
            testResults.map( testResult =>  u+"/"+ex.id+"/")
                //+"/src/"+(ex.solutionFQCN.replaceAll("\\.","/"))+"."+ex.fileExtention
          }).flatten

          val exSheetDir = config.exBaseDir + ex.id +"/sheet"+sheetId+ "/"
          mkdir(exSheetDir)
          val resultDir=exSheetDir+"tgzResult/"
          mkdir(resultDir)
          val lang = if (ex.language==Language.C) "c" else "java19"  

          import org.apache.http.HttpResponse;
          import org.apache.http.NameValuePair;
          import org.apache.http.client.HttpClient;
          import org.apache.http.client.entity.UrlEncodedFormEntity;
          import org.apache.http.client.methods.HttpGet;
          import org.apache.http.client.methods.HttpPost;
          import org.apache.http.impl.client.DefaultHttpClient;
          import org.apache.http.message.BasicNameValuePair;
          import java.io.BufferedReader;
          import java.io.DataOutputStream;
          import java.io.InputStreamReader;

          var subs =  submission.foldLeft(""){(x,y)=>x+" "+y.replaceAll("\\ ","")};
          println(subs)

	  val url = "http://localhost:9090/tgzSubmissions";
          //val client = new DefaultHttpClient();
          import org.apache.http.impl.client.HttpClientBuilder
          val client = HttpClientBuilder.create().build();

	  val con = new HttpPost(url);
	  con.setHeader("User-Agent", "subato");


          val params = new java.util.ArrayList[NameValuePair]();
          params.add(new BasicNameValuePair("programs",subs))
          params.add(new BasicNameValuePair("resultDir", resultDir));
          params.add(new BasicNameValuePair("dir", resultDir));
          params.add(new BasicNameValuePair("language", lang));
          con.setEntity(new UrlEncodedFormEntity(params));
          val response = client.execute(con);
          val rd = new BufferedReader(
                        new InputStreamReader(response.getEntity().getContent()));

	  val out = new java.io.StringWriter()
	  var line = rd.readLine()
	  while (line != null) {
	    out.append(line)
            out.append("\n");
            line = rd.readLine()
	  }


          "#tarOutput *" #> <pre>{ out.toString}</pre>  &
          "#downloadResult *" #> (<a href={"/serveTGZResult/"+ex.id+"/"+sheetId}>{locStr("download")}</a>) 
        } else{
          "*" #> <span>{loc("your not allowed to start test for plagiarism")}</span>
        }
      }
      case _ =>  "*" #> <span>{loc("noExerciseSpecified")}</span>
    }
  
  }

  def mkdir(file:String) {
    import java.io._
    var dir = file
    if (dir.endsWith("/")) {
      dir = dir.slice(0, dir.length - 1)
    }
    val parts = dir.trim.split('/')
    for (i <- 0 to parts.size - 1) {
      val f = new File(parts.slice(0, i).foldLeft("./")(_ + "/" + _))
      if (!f.exists) {
	f.mkdir()
      }
    }
    if (file.endsWith("/")) {
      new File(file).mkdir
    }
  }

  def simpleRun2(prog:List[String],directory:String,outP:java.io.Writer) = {
    var p = Runtime.getRuntime().exec(prog.foldLeft("")((r,n) => r+" "+n),Array[String](),new java.io.File(directory))
    p.waitFor()
  }

  def simpleRun(prog:List[String],directory:String,outP:java.io.Writer) = {
    import scala.collection.JavaConverters._
    val pb = new ProcessBuilder(prog.asJava)
    val env = pb.environment()
    env.put("PATH", "/usr/bin:/bin")
   // println("simple run: "+prog)

   // println("out to: "+directory+"/outError.xml")
    pb.directory(new java.io.File(directory))

    val p = pb.start()
    new Thread(){
      override def run(){
 //       val outError = new java.io.FileWriter(directory+"/outError.txt")
 //       val out = new java.io.BufferedWriter(new java.io.FileWriter(directory+"/out.txt"))
        try{
          val errorOutput = new java.io.InputStreamReader(p.getErrorStream)

          var i = errorOutput.read
          while (i>0){
//            outError.write(i.asInstanceOf[Char])
            outP.write(i.asInstanceOf[Char])
            i = errorOutput.read
          }

          val output = new java.io.InputStreamReader(p.getInputStream)

          var i2 = output.read
          while (i2>0){
//            out.write(i2.asInstanceOf[Char])
            outP.write(i2.asInstanceOf[Char])
            i2 = output.read
          }
          p.waitFor(5,java.util.concurrent.TimeUnit.MINUTES);
          println(p.exitValue)
        }finally{
   //       out.close
          //          outP.close
          println(p.exitValue)
    //      outError.close
        }
      }
    }.run()


    println("did it with: "+prog)
    println(p.exitValue)
    p.exitValue
  }

}


