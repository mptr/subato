package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import scala.xml.{NodeSeq, Text}
import net.liftweb.common.Logger
import net.liftweb.http.SessionVar

class ListCardsSnippet(val c:Box[CardBox])
    extends AbstractListCardsSnippet
    with SWrapperL10n
    with XmlConfigDependency 
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override val course = c
}

trait AbstractListCardsSnippet {
  this:ModelDependencies
      with L10n  =>

  protected val params:Params
  protected val course:Box[CardBox]

  def list = {
    (course) match {
      case Full(c) => {
        if (currentUser.isLecturerIn(c.course)){
        "#row" #> 
           c.cards.map((ex) =>
             <tr class="myrow">
               <td class="myrow-item" id="name">{ex.name}</td>
               <td id="view"><a href={"/multiplecard/view/"+ex.id}>{loc("view")}</a></td>
               <td id="edit"><a href={"/multiplecard/edit/"+ex.id}>{loc("edit")}</a></td>
               <!-- <td id="delete"><a href={"/multiplecard/delete/"+ex.id}>{loc("delete")}</a></td> -->
             </tr>) &
          "#courseName *" #> (c.name ) 
        }else{
          "#courseName *" #> (c.name )
        }
      }
      case _ =>  "*" #> <span>{loc("noCardBoxSpecified")}</span>
    }
  }
}
