package name.panitz.subato.snippet

import net.liftweb.common.{Box, Empty}

import name.panitz.subato.model._
import name.panitz.subato._

import name.panitz.subato.model.mapper.Granted

import java.util.Date

import net.liftweb.common.Full
import scala.xml.NodeSeq
import net.liftweb.util.Helpers._

class GrantExerciseSheetForStudentSnippet(e:Box[ExerciseSheet])
    extends AbstractGrantExerciseSheetForStudentSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  override protected var termId:String = null
  override protected var uid:String = null
  override protected val params = ParamsImpl
  override protected val exerciseSheet = e

  def this() {
    this(Empty)
  }
  def this(e:(String,String,Box[ExerciseSheet])) = {
    this(e._3)
    this.termId = e._2
    this.uid = e._1
  }
}

trait AbstractGrantExerciseSheetForStudentSnippet {
  this:ModelDependencies
    with L10n  =>

  protected val params:Params
  protected val exerciseSheet:Box[ExerciseSheet]
  protected var termId:String
  protected var uid:String

  import scala.xml.Text;
  def grantSheet = exerciseSheet match {
    case Full(sheet) => {
      if (currentUser.isTutorIn(sheet.course.id,termId.toLong)
      ) {
        import name.panitz.subato.model.mapper.{ Granted => MG }
        import net.liftweb.mapper.By

        val grants = MG.findAll(By(MG.uid, uid),By(MG.term, termId.toLong),By(MG.exerciseSheet,sheet.id))
        if (grants.isEmpty) {
          var gr = new Granted(uid,currentUser.uid.openOrThrowException("uid is not set for User"),new Date(),sheet.id,sheet.course.id,termId.toLong)
          gr.save
          "#ok" #> Text(gr.lecturer.get+ " ("+gr.datetime.get+")")
        }else{
          for (grant<-grants){
            var success = grant.delete_!
          }
          "#ok" #> Text("not granted")
        }
      }else{
        "#ok" #> Text("not granted")
      }
    }
    case _ => loc("noSuchExerciseSheet")
  }

}
