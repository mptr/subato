package name.panitz.subato.snippet

import java.time.temporal.{ChronoUnit => CU}

import name.panitz.subato.model._
import name.panitz.subato.model.mapper.Ontology
import name.panitz.subato.model.mapper.OntologyEntry
import name.panitz.subato.model.Progress._
import name.panitz.subato._
import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{S, SHtml, SessionVar, StatefulSnippet}
import net.liftweb.common.Logger
import net.liftweb.mapper.By
import java.time.{LocalDateTime => LDT}

import net.liftweb.util.CssSel
import net.liftweb.http.SHtml._
import scala.xml.{NodeSeq, Text, Elem}

/* This snippet is used to administrate single card, ie. edit them, edit answers and 
 adminsitrate the ontology entries.
 
 This is granted to all lecturers.

  The resulting page serves this purpose until there is a nice and perfect way to do these things in SLS Studio.
*/
class SingleCardSnippet(val c:Box[Card])
    extends AbstractSingleCardSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  override protected val params = ParamsImpl
  override protected val card = c
}

import net.liftweb.http.S

trait AbstractSingleCardSnippet extends Logger
    with AbstractCardSnippet
    with AnswerHandler{
  this: ModelDependencies with L10n =>

  protected val params:Params
  protected val card:Box[Card]
  

  def showMultiple: CssSel = {
    if (!currentUser.isLecturer){
      S.redirectTo("/")
    }
    card match { // show in the current selected card box
      case Full(c) =>  // when the box contains something then
        var selectedOntology = 0L

        /* Avoids double entries. */
        def newOntologyEntry(){
          val oe = new OntologyEntry().ontology(selectedOntology).flashcard(c.id)
          val ont = ontologys.find(selectedOntology) openOr null
          if (!c.ontology.contains(ont)){
            oe.save()
          }
        }

        /* Will delete several entries of the same ontology item. */
        def deleteOntEntry(cid :Long,oid:Long){
          mapper.OntologyEntry.findAll(
            By(mapper.OntologyEntry.flashcard, cid)
              ,By(mapper.OntologyEntry.ontology, oid)
          ).map(entry => entry.delete_!)
        }

        // TODO create nicer buttons....

        c.initialize() //for generic cards get a new instance for display (but generic card will be edited)
        "#number *" #> c.id &
        "#newentry *" #>
          <div>{
            selectObj(Ontology.findAll.map(x => new SelectableOption(x.id.get, x.name.get))
              , net.liftweb.common.Empty, (s:Long) => selectedOntology=s)
          }{button("new ontology entry", ()=> newOntologyEntry())
          }</div> &
        "#title *" #> c.name &
        "#edit *" #>  <a href={c.editUrl}>Edit Card</a> &
        "#editAnswerLinks *" #>
          c.answers.map((a) =>
            <div>
              {if (a.isCorrect) <span>(correct)</span> }
              {a.text}<br /><a href={"/answer/edit/"+a.id}>Edit</a>
            </div>) &
        "#ontology *" #>
          c.ontology.map((a) =>
            <span><a href={"/ontology/edit/"+a.id}>{a.name}</a> ({button("delete",()=>deleteOntEntry(c.id,a.id))})</span>) &
        c.snippet(this, c.id)
              
      case _ => S.redirectTo("/")
    }
  }


  override def computeNeededTime: Long = 0
  override def eval: Any = {}
  override def getBox: scala.xml.Elem = <span></span>
  override def getLastCard: Option[name.panitz.subato.model.Card] = None
  override def getPlayedCardResultIndex: Map[Long,List[scala.xml.Elem]] = Map()
  override def getSubmission: Option[name.panitz.subato.model.Submission] = None
  override def getTextAnswer: String = ""
  override def getTextAnswers: Seq[String] = List()
  override def isSolutionCorrect(value: Boolean): Unit = {}
  override def isSolutionCorrect_? : Boolean = false

  override def isSolutionPartialCorrect(value: Boolean): Unit = {}
  override def isSolutionPartialCorrect_? : Boolean = false
  override def playedCardResultIndex(newMap: Map[Long,List[scala.xml.Elem]]): Unit = {}
  override def redirectTo(link: String): net.liftweb.util.CssSel = S.redirectTo(link)
  override def setNextCardLink(url: String,urlLabel: String): net.liftweb.util.CssSel = S.redirectTo(".") 
  override def processResult(cardId: Long,p: name.panitz.subato.model.Progress.Progress): Unit = {}
  override def solutionSubmitted(sol: name.panitz.subato.model.Solution): Unit = {}


}
