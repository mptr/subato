package name.panitz.subato.snippet

import scala.xml.{Elem, Text}
import name.panitz.subato.{L10n, SWrapperL10n}
import name.panitz.subato.model._
import net.liftweb.common.{Box, Empty, Full, Logger}
import net.liftweb.http.{SHtml, StatefulSnippet}
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._
      
trait AbstractAdminFlashcardCoursesSnippet
    extends StatefulSnippet
    with SWrapperL10n
    with MapperModelDependencies
    with Logger {
  
  override def dispatch(): PartialFunction[String, CssSel] = {
    case "list" => list
  }

  def list(): CssSel = {
    "#title" #> <h2>{loc("flashcardcourseCreatedByYou")}</h2> &
    "#content" #>
    <table>
      <tr></tr>
        {
          flashcardCourses.findAllCreatedBy(currentUser.uid.openOrThrowException("")).map(c =>
            <tr>
              <td><b>{breakIftoLong(c.title)}</b></td>
              <td><b>{if (c.isPrivate_?) "private" else "public"}</b></td>
              <td><a href={"/DeleteFlashCardCourse/" + c.id}>{loc("delete")}</a></td>
              <td><a href={"/ExportFlashCardCourse/" + c.id}>{loc("exportXml")}</a></td>
              <td>{SHtml.submit(loc("toggle privacy").text, () => c.togglePrivacy, ("backgroundcolor" -> "red"))}</td>
            </tr>
          )
        }
    </table>
    <div>
      <tr>
        <td><a href={"/ImportFlashCardCourse"}>{loc("importFlashCardCourse")}</a></td>
        <td></td>      
        <td><a href={"/FlashcardCourseStudio"}>{loc("createFlashCardCourseManually")}</a></td>
      </tr>
    </div>
  }

  def breakIftoLong(str: String): String = if (str.length() > 25) str.substring(0, 22) + "..." else str

}
      
class AdminFlashcardCoursesSnippet extends AbstractAdminFlashcardCoursesSnippet {}
