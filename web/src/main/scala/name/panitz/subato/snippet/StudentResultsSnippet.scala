package name.panitz.subato.snippet

import net.liftweb.common.{Box, Empty}

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.Full
import scala.xml.NodeSeq
import net.liftweb.util.Helpers._

class StudentResultsSnippet(e:Box[Exercise])
    extends AbstractStudentResultsSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  override protected var termId:String = null
  override protected var uid:String = null
  override protected val params = ParamsImpl
  override protected val exercise = e

  def this() {
    this(Empty)
  }
  def this(e:(String,String,Box[Exercise])) = {
    this(e._3)
    this.termId = e._2
    this.uid = e._1
  }
}

trait AbstractStudentResultsSnippet {
  this:ModelDependencies
    with L10n  =>

  protected val params:Params
  protected val exercise:Box[Exercise]
  protected var termId:String
  protected var uid:String

  def resultsByStudTermAndEx = exercise match {
    case Full(ex) => {
      var cuid = currentUser.uid.openOrThrowException("nouserloggedIn")
      if (currentUser.isTutorIn(ex.course.id,termId.toLong)||uid==cuid) {
        import net.liftweb.mapper._
        val subs = submissions.find(uid,ex,ex.course.id,termId.toLong)

        if (exercise.map(_.isGraded).openOr(false)){
          var rows = subs.map( sub => {
            <div>
            <h2 style="background: #AAAAAA;color: #000000;">{loc("submission")}</h2>
            {if (currentUser.isTutorIn(ex.course.id,termId.toLong))
              <div><a onclick="startSpinner()"  href={"/AbnahmeTest/"+ex.id+"/"+sub.id}>{loc("startCertificationTest")}</a></div>
            }

            {sub.asHtml(ex)}
            {if (currentUser.isTutorIn(ex.course.id,termId.toLong))
              <div><a href={"/submission/edit/"+sub.id}>{loc("comment")}</a></div>
            }
          </div>
          })
          "#rows *" #> rows
        }else{
          "#rows *" #> <span />
        }
      } else {
        loc("accessDenied")
      }
    }
    case _ => loc("noSuchExercise")
  }
	
}
