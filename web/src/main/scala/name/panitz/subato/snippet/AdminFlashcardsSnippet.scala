package name.panitz.subato.snippet

import scala.xml.{Elem, Text}

import name.panitz.subato._
import name.panitz.subato.model._
import net.liftweb.common.{Box, Empty, Full, Logger}
import net.liftweb.http.{SHtml, StatefulSnippet}
import net.liftweb.util.CssSel
import net.liftweb.util.Helpers._

class AdminFlashcardsSnippet(val c:Box[CardBox])
    extends AbstractAdminFlashcardsSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override protected val cardBox = c

}

trait AbstractAdminFlashcardsSnippet
    extends StatefulSnippet
    with name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val cardBox:Box[CardBox]

  override def dispatch = {
    case "list" => list
    case "confirm" => confirm
  }

  def list:CssSel={
    if (currentUser.isLecturer){
      "#content *" #>
        courses
         .findAll
         .map((c) => list(c))
    }else{
       "#content *" #> "not allowed"
    }
  }

  def list(c:Course):Elem={
    <div>
    <h2>{c.name}</h2>
    <table><tr><th>{loc("name")}</th><th></th><th></th><th></th><th></th><th></th>
    </tr>
      {
        cardBoxes.findAllForCourseAndUser(c.id,currentUser.uid.openOrThrowException(""))
         .map((cb) =>
          <tr>
            <td><b>{cb.name}</b></td>
            <td><b>{if (cb.isPrivate) "private" else "public"}</b></td>
            <td><a href={"/DeleteCardbox/"+cb.id}>{loc("delete")}</a></td>
            <td><a href={"/exportCardbox/"+cb.id+"/history"}>{loc("exportXml")}</a></td>
            <td><a href={"/exportQTI/"+cb.id+"/history"}>{loc("exportQTI")}</a></td>
            <td>{SHtml.submit(loc("toggleprivacy").text,() =>{
              cb.isPrivate = !cb.isPrivate
              cb.save
            })}</td>
            <td><a href={"/ListCards/"+cb.id}>{loc("showCards")}</a></td>
            <td><a href={"/CardBox/"+cb.id}>{loc("view")}</a></td>
           </tr>
        )
      }</table>
      <a href={"/ImportCardBox/"+c.id}>{loc("importFlashcardBox")}</a>
    <div style="height: 2em;" />
    </div>

  }

  def confirm:CssSel={
    cardBox match {
      case Full(cb) => {
        def deleteIt{
          cb.delete
          redirectTo("/AdminFlashcards")
        }
        def dontDeleteIt{
          redirectTo("/AdminFlashcards")
        }


        if (currentUser.isLecturerIn(cb.course.id)||currentUser.uid.openOrThrowException("")==cb.uid){
          "#content *" #>
            <div>
             <h2>{loc("delete")} {loc("flashcards")}</h2>
             <div>{loc("deleteFlashcardsWarning")}</div>
              {SHtml.button(loc("no"),dontDeleteIt _, ("type","submit"))}
              {SHtml.button(loc("yes"),deleteIt _, ("type","submit"))}
            </div>
        }else "*" #> <span>{loc("noCardboxSpecified")}</span>
      }
      case _ =>  "*" #> <span>{loc("noCardboxSpecified")}</span>
    }
  }
}
