package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.common.Logger
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import net.liftweb.http.S
import net.liftweb.util.CssSel
import scala.xml.{NodeSeq, Text, Elem}

class AdminSurveysSnippet(val c:Box[Survey])
    extends AbstractAdminSurveysSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }
  override protected val params = ParamsImpl
  override protected val survey = c

}

trait AbstractAdminSurveysSnippet
    extends StatefulSnippet
    with name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
      with L10n =>

  protected val params:Params
  protected val survey:Box[Survey]

  override def dispatch = {
    case "list" => list
    case "confirm" => confirm
    case "confirmClear" => confirmClear
  }

  def list:CssSel={
    if (currentUser.isLecturer){
      "#content *" #>
        courses
         .findAll
         .map((c) => list(c))
    }else{
       "#content *" #> "not allowed"
    }
  }

  def list(c:Course):Elem={
    <div>
    <h2>{c.name}</h2>
    <table><tr><th>{loc("name")}</th><th></th><th></th><th></th><th></th><th></th>
    </tr>
      {
        surveys.findAllForCourse(c.id)
         .map((cb) =>
          <tr>
            <td><b>{cb.name}</b></td>
            <td><b>{if (cb.isPrivate) "private" else "public"}</b></td>
            <td><b>{if (cb.isActive) "active" else "not active"}</b></td>
            <td><a href={"/DeleteSurvey/"+cb.id}>{loc("delete")}</a></td>
            <td><a href={"/ClearSurvey/"+cb.id}>{loc("clear")}</a></td>
            <td>{SHtml.submit(loc("toggleprivacy").text,() =>{
              cb.isPrivate = !cb.isPrivate
              cb.save
            })}</td>
            <td>{SHtml.submit(loc("toggleactive").text,() =>{
              cb.isActive = !cb.isActive
              cb.save
            })}</td>
            <td><a href={"/Survey/"+cb.id}>{loc("view")}</a></td>
            <td><a href={"/SurveyResult/"+cb.id}>{loc("results")}</a></td>
           </tr>
        )
      }</table>
      <a href={"/ImportSurvey/"+c.id}>{loc("importSurvey")}</a>
    <div style="height: 2em;" />
    </div>

  }

  def confirm:CssSel={
    survey match {
      case Full(cb) => {
        def deleteIt{
          cb.delete
          redirectTo("/AdminSurveys")
        }
        def dontDeleteIt{
          redirectTo("/AdminSurveys")
        }


        if (currentUser.isLecturerIn(cb.course.id)){
          "#content *" #>
            <div>
             <h2>{loc("delete")} {loc("survey")}</h2>
             <div>{loc("deleteSurveysWarning")}</div>
              {SHtml.button(loc("no"),dontDeleteIt _, ("type","submit"))}
              {SHtml.button(loc("yes"),deleteIt _, ("type","submit"))}
            </div> 
        }else 
         "*" #> <span>{loc("notAllowedForUser")}</span>
      }
      case _ =>  "*" #> <span>{loc("noSurveySpecified")}</span>
    }
  }


  def confirmClear:CssSel={
    survey match {
      case Full(cb) => {
        def deleteIt{
          cb.clear
          redirectTo("/AdminSurveys")
        }
        def dontDeleteIt{
          redirectTo("/AdminSurveys")
        }


        if (currentUser.isLecturerIn(cb.course.id)){
          "#content *" #>
            <div>
             <h2>{loc("clear")} {loc("survey")}</h2>
             <div>{loc("clearSurveysWarning")}</div>
              {SHtml.button(loc("no"),dontDeleteIt _, ("type","submit"))}
              {SHtml.button(loc("yes"),deleteIt _, ("type","submit"))}
            </div> 
        }else 
         "*" #> <span>{loc("notAllowedForUser")}</span>
      }
      case _ =>  "*" #> <span>{loc("noSurveySpecified")}</span>
    }
  }


}
