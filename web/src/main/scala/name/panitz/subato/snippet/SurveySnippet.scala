package name.panitz.subato.snippet

import name.panitz.subato.model._
import name.panitz.subato.model.Progress._
import name.panitz.subato._

import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{StatefulSnippet, SHtml}
import net.liftweb.http.SessionVar
import net.liftweb.common.Logger
import net.liftweb.mapper.By

import scala.xml.{NodeSeq, Text}

class SurveySnippet(val c:Box[Survey])
    extends AbstractSurveySnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  override protected val params = ParamsImpl
  override protected val survey = c
}

trait AbstractSurveySnippet  extends StatefulSnippet
    with  AnswerHandler
    with name.panitz.subato.XmlConfigDependency
    with Logger{
  this:ModelDependencies
      with L10n =>


  abstract class StateVar[T](defV:T) extends SessionVar[Map[Long,T]](Map()) {
    def <= (ep : T)={
      survey match{
        case Full(cb) => set(get + (cb.id -> ep))
        case _ => 
      }
    }
    def >= ={
      survey match{
        case Full(cb) =>  get.get(cb.id).getOrElse(defV)
        case _ => defV
      }
    }
  }

  protected val params:Params
  protected val survey:Box[Survey]
  override def dispatch = {
    case "show" => show
    case "results" => showResults
    case "showSurveys" => showSurveys
    case "thanks" => thanks

  }


  object allreadySubmitted extends StateVar[Boolean](false)

  override def processResult(id:Long, res : Progress) ={
  }

  def solutionSubmitted(sol:Solution)={
    survey match{
      case Full(s) =>
        allreadySubmitted <= true
        sol match{
          case WordListSolution(ws) => 
            val a = s.addAnswer
            for (w <- ws) a.addAnswer(w)
        }
        redirectTo("/SurveyThanks/"+s.id)
      case _ =>

    }
  }



  def show:net.liftweb.util.CssSel={
    survey match{
      case Full(s) =>
        if (allreadySubmitted.>=) redirectTo("/SurveyResult/"+s.id)
        s.card.snippet(this, s.id)&
        "#hidden" #> SHtml.hidden(()=> s.card.id )
      case _ => "#t" #> loc("noCardBoxSpecified")
    }
  }

  def thanks={
    survey match{
      case Full(s) =>
        "#title *" #> s.card.name &
        "#content *" #> loc("thanksForSurveyParticipation") 
      case _ => "#t" #> loc("noSurveySpecified")     
    }
  }


  def showResults={
    survey match{
      case Full(s) =>
        val card = s.card
        val allGivenSelections = s.givenAnswers.map(_.selectedAnswers).flatten
        val possibleAnswers = card.answers.map((a) => (a.text,allGivenSelections.filter((x)=>x==a.text.text).size  ))
        val g = s.givenAnswers.size
        "#title *" #> card.name &
        "#question *" #> card.exerciseText &
        "#resultTable" #>
        <div >
          <div style="clear:left;">{locStr("overall")}: {g}<span style="width: 500px; height:30px;  background: #AAAAAA;  border: 3px solid #FFFFFF;float:left;"></span></div>
          {possibleAnswers.map((x) => x match  {case (t,n) => <div style="clear:left;">{t.text}: {n.toString}<span style={"width: "+(if (g==0) 0 else (n*500/g))+"px; height:30px;  background: #CCCCCC;  border: 3px solid #FFFFFF;float:left;"}></span></div>})}</div>
      case _ => "#t" #> loc("noSurveySpecified")     
    }
  }


  def showSurveys:net.liftweb.util.CssSel={
    "#rows *" #> {
      val surveys = name.panitz.subato.model.mapper.Survey
        .findAll(By(mapper.Survey.isPrivate,false))
        .map(new MapperSurvey(_))
    //        .filter(_.isPublic)
      if (surveys.isEmpty) <div>{locStr("noActiveSurvey")}</div>
      else 
        surveys.map((b)=> <a href={"/Survey/"+b.id}>{b.name+" ("+b.course.name+")"}</a>)
    }
  }


}



