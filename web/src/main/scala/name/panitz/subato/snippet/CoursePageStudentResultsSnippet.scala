package name.panitz.subato.snippet

import net.liftweb.common.{Box, Empty}

import name.panitz.subato.model._
import name.panitz.subato._

import net.liftweb.common.Full
import scala.xml.NodeSeq
import net.liftweb.util.Helpers._

class CoursePageStudentResultsSnippet(e:Box[CoursePage])
    extends AbstractCoursePageStudentResultsSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  override protected var uid:String = null
  override protected val params = ParamsImpl
  override protected val coursePage = e

  def this() {
    this(Empty)
  }
  def this(e:(String,Box[CoursePage])) = {
    this(e._2)
    this.uid = e._1
  }
}

trait AbstractCoursePageStudentResultsSnippet {
  this:ModelDependencies
    with L10n  =>

  protected val params:Params
  protected val coursePage:Box[CoursePage]
  protected var uid:String


  def studentResult = coursePage match {
    case Full(cp) => {
      if (cp.isTutorIn
        || currentUser.uid.openOr("")==uid
      ) {
        import net.liftweb.mapper._
        val result = cp.courseExerciseSheets.sortBy(_.number).map(sheet => {
          val exercises = sheet.sheetExercises.map(_.exercise)
          val termId = cp.term.id
          val subs = exercises
               .filter(_.isGraded)
               .map((ex) => (ex,submissions.findBestTested(uid, sheet.course.id,ex.id,cp.term.id)))

          (<tr>
            <th style="background-color:green;color:white;" colspan="2">{locStr("exerciseSheet")+" "+sheet.number+": "+sheet.name}</th>
            <th style="background-color:green;color:white;" colspan="8">{
              if (currentUser.isTutorIn(sheet.course.id,termId.toLong))
                  {<span>{sheet.grantButton(uid)}</span>}
              else{<span>{sheet.isGranted(uid).openOr("not granted")}</span>}
            }
            </th>
            </tr>
            ::
            <tr>
              <th style="background-color:green;color:white;" colspan="2">({sheet.getPoints(uid)+"/"+sheet.reachablePoints})</th>
              <th style="background-color:green;color:white;" colspan="8">{
              if (currentUser.isTutorIn(sheet.course.id,termId.toLong))
                 <span><a style="color:white;" href={"/PointsForSheetAndUser/"+sheet.id+"/"+uid}>{locStr("pointsEntry")}</a></span>
              else 
                 <span></span>
              }
              </th>
            </tr>
          :: {subs.map({case (ex,sub) =>
            (<tr>
              <td>{if (!sub.isEmpty) studentLink(ex.id,uid,termId+"") else uid}</td>
              <td>{ex.name}</td>
              <td>{sub.map(_.tests.toString).openOr("/")}</td>
              <td>{sub.map(_.failures.toString).openOr("/")}</td>
              <td>{sub.map(_.errors.toString).openOr("/")}</td>
              <td>{sub.map((x)=>(x.allocations-x.frees).toString).openOr("/")}</td>
              <td>{sub.map(_.stylecheckErrors.toString).openOr("/")}</td>
              <td>{sub.map((x)=>(x.tests - x.failures - x.errors).toString).openOr("/")}</td>
              <td>{sub.map(_.score.toString).openOr("/")}</td>
              <td>{
               var numberOfCommets = submissions.commentsAvailable(uid, ex)
               if (numberOfCommets>0)
                 <span style="background-color: yellow;">({numberOfCommets} {locStr("commentAvailable")})</span>
              }</td>
             </tr>
            )})})
        }).flatten
        "#row " #> result &
        "#title *" #> <span>{cp.course.name+" "+cp.term.name+" "+uid}</span> &
        "#attendances *" #> attendances.findByCoursePageAndUser(cp,uid).map((x) => <span>{x.date}</span>) &
        "#addAttendanceToday" #>  {
          val id = cp.id
          val today = java.time.LocalDate.now
          val atts = attendances.findByCoursePageAndUser(cp,uid).map(_.date)
          if (!atts.contains(today)&&currentUser.isTutorIn(cp.course.id,cp.term.id)){
            <span>
            <style>.button {{
              font: bold 11px Arial;
              text-decoration: none;
              background-color: #EEEEEE;
              color: #333333;
              padding: 2px 6px 2px 6px;
              border-top: 1px solid #CCCCCC;
              border-right: 1px solid #333333;
              border-bottom: 1px solid #333333;
              border-left: 1px solid #CCCCCC;
            }}
            </style>
            <script>
            function ajaxcallToday{uid}{id}(element) {{
              var xhttp = new XMLHttpRequest();
              xhttp.onreadystatechange = function() {{
                if (this.readyState == 4 <![CDATA[&&]]> this.status == 200) {{
                  var el = document.createElement( "html" );
                  el.innerHTML=this.responseText;
                  //element.innerText=el.textContent;
                  element.remove();
                  var li = document.createElement("li");
                  li.innerText=el.textContent;
                  //var textnode = document.createTextNode(this.responseText);         
                  //node.appendChild(el);
                  document.getElementById("attendanceList").appendChild(li);
                }}
              }};
              xhttp.open("GET", "{"/subato/AttendStudentInCourse/" +id+ "/" + uid}", true);
              xhttp.send();
            }}</script>
            <span onclick={"ajaxcallToday"+uid+id+"(this)"} class="button">Heute Anwesend</span>
            </span>
          }else <span></span>
        }
      }
      else  "#row *" #> <span />
    }
    case _ => loc("noSuchExerciseSheet")
  }

  private def studentLink(exId:Long, uid:String,termId:String):NodeSeq = {
    <a href={"/ExerciseResults/" +exId+"/"+termId  + "/" + uid}>{uid}</a>
  }

}
