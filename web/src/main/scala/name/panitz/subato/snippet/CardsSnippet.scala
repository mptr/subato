package name.panitz.subato.snippet

import java.time.temporal.{ChronoUnit => CU}

import name.panitz.subato.model._
import name.panitz.subato.model.Progress._
import name.panitz.subato._
import net.liftweb.common.{Box, Empty}
import net.liftweb.common.Full
import net.liftweb.util.Helpers._
import net.liftweb.http.{S, SHtml, SessionVar, StatefulSnippet}
import net.liftweb.common.Logger
import net.liftweb.mapper.By
import java.time.{LocalDateTime => LDT}

import net.liftweb.util.CssSel

import scala.xml.{NodeSeq, Text, Elem}

class CardsSnippet(val c:Box[CardBox])
    extends AbstractCardsSnippet
    with SWrapperL10n
    with MapperModelDependencies {

  def this() {
    this(Empty)
  }

  override protected val params = ParamsImpl
  override protected val cardbox = c
}

trait AbstractCardsSnippet  extends StatefulSnippet
    with  AnswerHandler
    with name.panitz.subato.XmlConfigDependency
    with Logger
    with AbstractCardSnippet {
  this: ModelDependencies with L10n =>


  abstract class StateVar[T](defV: T) extends SessionVar[Map[Long,T]](Map()) {
    def <= (ep : T) = {
      cardbox match {
        case Full(cb) => set(get + (cb.id -> ep))
        case _ =>
      }
    }
    def >= = {
      cardbox match {
        case Full(cb) => get.get(cb.id).getOrElse(defV)
        case _        => defV
      }
    }
  }

  protected val params:Params
  protected val cardbox:Box[CardBox]
  override def dispatch = {
    case "showMultiple" => showMultiple
    case "Results" => showResults
    case "showBoxes" => showBoxes
    case "CreateForm" => createBoxForm
    case "CreateCardForm" => createCardForm
    case "statistics" => showStatisticalData
  }

  override def isSolutionCorrect_? : Boolean = isSolutionCorrect.get
  override def isSolutionCorrect(value: Boolean): Unit = isSolutionCorrect set value
  override def isSolutionPartialCorrect_? : Boolean = isSolutionPartialCorrect.get
  override def isSolutionPartialCorrect(value: Boolean): Unit = isSolutionPartialCorrect set value
  override def getLastCard: Option[Card] = lastCard.>=
  override def getSubmission: Option[Submission] = submission.>=
  override def getTextAnswers: Seq[String] = textAnswers.get
  override def getTextAnswer: String = textAnswer.get
  override def getPlayedCardResultIndex: Map[Long, List[Elem]] = playedCardResultIndex.get
  override def playedCardResultIndex(newMap: Map[Long, List[Elem]]): Unit = playedCardResultIndex set newMap
  override def setNextCardLink(url: String, label: String): CssSel = {
    "#selectionItem *" #> List(<a href={url}>{label}</a>)
  }

  override def processResult(id: Long, res: Progress) = processResult(id, res, true)

  override def processResult(id: Long, res: Progress, actualizeStatisticalData: Boolean) = {
    isSolutionCorrect.set(false)
    isSolutionPartialCorrect.set(false)
    res match {
      case Correct => isSolutionCorrect.set(true)
      case PartialCorrect => isSolutionPartialCorrect.set(true)
      case _ =>
    }
    cardbox match {
      case Full(cb) => {
        if (actualizeStatisticalData) actualizeCardStatistics(id)
        redirectTo("/CardResult/" + cb.id)
      }
      case _ =>
    }
  }

  def actualizeCardStatistics(id: Long): Unit = {
    val neededTime = computeNeededTime()
    resolutionTime.set(neededTime)
    if(isSolutionCorrect) statistics.incGoodAnswerNum(id, neededTime)
    else if(isSolutionPartialCorrect) statistics.incAlmostCorrectAnswerNum(id, neededTime)
    else statistics.incWrongAnswerNum(id, neededTime)
  }

  override def computeNeededTime(): Long = this.cardDisplayTime.get.until(LDT.now(), CU.SECONDS)


  def solutionSubmitted(sol:Solution)={
    sol match{
      case TextSolution(text) => textAnswer.set(text)
      case WordListSolution(ws) => textAnswers.set(ws)
      case ProgrammingSolution(sub) => submission <= Some(sub)
    }
  }

  object cards extends StateVar[Option[List[Card]]](None)

  object playedCardResultIndex extends SessionVar[Map[Long,List[scala.xml.Elem]]](Map()) {}

  object isSolutionCorrect extends SessionVar[Boolean](false) {}

  object isSolutionPartialCorrect extends SessionVar[Boolean](false) {}

  object lastTimeAsResult extends StateVar[Boolean](true)

  object points extends StateVar[Long](0)

  object playedQuestions extends StateVar[Long](0)

  object lastCard extends StateVar[Option[Card]](None) {}

  object submission extends StateVar[Option[Submission]](None) {}

  object lastQuestion extends SessionVar[Long](-1) {}

  object cardDisplayTime extends SessionVar[LDT](LDT.now())

  object resolutionTime extends SessionVar[Long](0)

  object textAnswer extends SessionVar[String]("") {}

  object textAnswers extends SessionVar[List[String]](List()) {}

  def showBoxes:net.liftweb.util.CssSel={
    "#rows *" #> name.panitz.subato.model.mapper.CardBox
      .findAll(By(mapper.CardBox.isPrivate,false))
      .map(new MapperCardBox(_))
      .filter(_.isPublic)
      .map((b)=> <a href={"/CardBox/"+b.id}>{b.name+" ("+b.course.name+")"}</a>)
  }


  val resultTemplate = <html>
    <head>
      <meta charset="UTF-8" />
      <link rel="stylesheet" href="/subato/classpath/blueprint/screen.css" type="text/css" media="screen, projection"/>
      <link rel="stylesheet" href="/subato/classpath/blueprint/print.css" type="text/css" media="print"/>

      <link rel="stylesheet" href="/subato/classpath/blueprint/plugins/fancy-type/screen.css" type="text/css" media="screen, projection"/>
      <link type="text/css" rel="stylesheet" href="/subato/assets/css/app.css"/>
      <script type="text/javascript" src="/subato/classpath/jquery.js" id="jquery"></script>
      <script type="text/javascript" src="/subato/classpath/json.js" id="json"></script>
      <script type="text/javascript" src="/subato/classpath/json.js" id="json"></script>
      <script type="text/javascript" src="/subato/classpath/fobo/jquery.js" id="jquery"></script>


      <script src="/subato/static/editarea_0_8_2/edit_area/edit_area_full.js" type="text/javascript" language="javascript"></script>

      <script src="https://cdn.jsdelivr.net/npm/prismjs@1.19.0/prism.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/prismjs@1.19.0/plugins/line-numbers/prism-line-numbers.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/prismjs@1.19.0/plugins/autoloader/prism-autoloader.js"></script>
      <script>Prism.plugins.autoloader.languages_path = 'https://cdn.jsdelivr.net/npm/prismjs@1.19.0/components/';</script>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/prismjs@1.19.0/themes/prism.min.css"/>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/prismjs@1.19.0/plugins/line-numbers/prism-line-numbers.min.css"/>




        <link rel="shortcut icon" href="/subato/favicon.ico" />
      <title data-lift="Menu.title">Subato</title>

      <link href="/subato/static/card.css" rel="stylesheet" />
    </head>

    <body>
      <div class="card">
        <div class="innercard">
          <div><div id="progress"></div></div>
          <div id="statistic"></div>
          <div class="question" style="clear:left;" id="question"></div>
          <div class="answer" id="givenAnswer"></div>
          <div class="answer" id="correctAnswer"></div>
          <div class="answer" id="explanation"></div>
          <div style="float: right;"><a href="/subato/"><small><img height="12px" src="/subato/icon.png" style ="vertical-align: middle;"/>Subato</small></a></div>
        </div>
      </div>
    </body>
  </html>

  override def getBox: scala.xml.Elem = {
    if (isSolutionCorrect_?)  <span class="greenbox"/>
    else if (isSolutionPartialCorrect_?) <span class="yellowbox"/>
    else <span class="redbox"/>
  }

  override def eval: Any = {
    playedQuestions <= (playedQuestions.>= +1)
    if (isSolutionCorrect_?)  points <= (points.>= +2)
    else if (isSolutionPartialCorrect_?)   points <= (points.>= +1)
  }

  import Severity._
  override def abort(to: String): Nothing = {
    def reinitialize(): Unit = {
      cards <= None
      isSolutionCorrect.set(false)
      isSolutionPartialCorrect.set(false)
      lastTimeAsResult <= true
      points <= 0
      playedQuestions <= 0
      lastCard <= None
      submission <= None
      lastQuestion.set(-1)
      cardDisplayTime.set(LDT.now())
      textAnswer.set("")
      textAnswers.set(List())

      cardbox match {
        case Full(cb) => playedCardResultIndex.set(playedCardResultIndex.get +  (cb.id -> List()))
        case _        =>
      }
    }

    reinitialize()
    PrettyAlerter.setMsg("Oops....", loc("cardSuddenlyDeleted").toString, Warn)
    S.redirectTo(to)
  }


  def showMultiple: CssSel = {
    textAnswers.set(List()) // answer is empty
    cardbox match { // show in the current selected card box
      case Full(cb) =>  // when the box contains something then
        cards.>= match { // show the current noticed list of cards
          case None => cards <= Some(cb.cards) // nothing noticed then store all cards of the box in some list
          case _ => // already noticed then do nothing
        }

        playedCardResultIndex.get.get(cb.id) match {
          case None => playedCardResultIndex.set(playedCardResultIndex.get +  (cb.id -> List()))
          case _ =>
        }

        val Some(cs) = cards.>= // get current cards from the sessionvar
        val Some(rs) = playedCardResultIndex.get.get(cb.id) // get current list of cardresults from the sessionvar

        if (cs.nonEmpty){ // Some cards to play ?
          val c = cs.head // take the next card
          c.initialize() //for generic cards get a new instance
          val fromResult = lastTimeAsResult.>= // was the last action an result display
          val lcid = lastCard.>= match{ // get id of the last played card from sessionvar
            case Some(lc) => lc.id
            case _ => -1
          }

          lastTimeAsResult <= false // the last action is not more an result display

          if(!c.exist_?) {
            this.abort("/")
          } else if (fromResult && (c.id != lcid)){ // when the last action was an result display and the last played card id is diff from the next
            //dann ab zur nächsten Karte
            cardDisplayTime.set(LDT.now()) // time investigation
            cards <= Some(cs.tail) // update the remaining card. the rest of the original list without the head
            lastCard <= Some(c) // update the last card played
            c.snippet(this, cb.id) & // produce an Csssel and complete them
            "#hidden" #> SHtml.hidden(()=> lastQuestion(c.id) ) &
            "#progress" #> (rs.reverse ++ cs.map((x)=> <span class="greybox" />))
          }else{
            lastCard.>= match{ // redisplay the already displayed lastcard
              case Some(c) =>
                c.snippet(this, cb.id)&
                "#hidden" #> SHtml.hidden(() => lastQuestion(c.id) ) &
                "#progress" #> (rs.reverse ++  <span class="greybox" />::cs.map((x)=> <span class="greybox" />))
              case _ =>  "#hidden" #> SHtml.hidden(()=> lastQuestion(c.id) )
            }
          }
        }else { // all cards from cardbox have been played
          cards <= None
          var result = points.>= * 100 / (2*playedQuestions.>=)
          "#question *" #> <div>Alle Fragen beantwortet.<br/> Sie haben {result.toString}% erreicht.</div> &
          "#selectionItem" #>
            List( <a href={"/CardBox/"+cb.id}>Noch einmal</a>, <a href="/index">Beenden</a>)&
          "#progress" #> (rs.reverse ++ cs.map((x)=> <span class="greybox" />))
        }
      case _ => this.abort("/")  // when the cardbox contains nothing or the cardbox owner delete the cardbox during the player session
    }
  }

  def showResults = {
    cardbox match {
      case Full(cb) =>
       try{
        val Some(cs) = cards.>=
        val Some(rs) = playedCardResultIndex.get.get(cb.id)

        val lcid = lastCard.>= match{
          case Some(lc) => lc.id
          case _ => -1
        }

        val Some(c) = lastCard.>=
        if (lcid != lastQuestion.is){redirectTo("/CardBox/"+cb.id)}
        if(! lastTimeAsResult.>= ){
          lastTimeAsResult<=true
          if (!c.isInstanceOf[MapperFreeTextCard]) eval
          val resultCSS=c.resultSnippet(this, cb.id)
          val resultPage = resultCSS.apply(resultTemplate)
          val variable = "sol"+c.id
          val box = <span><script type="text/javascript">{"<!--\nvar "+variable+" = `"+resultPage+"`\n-->"}</script><a href={"javascript:popup(600,800,"+variable+")"}>{getBox}</a></span>
          playedCardResultIndex.set(playedCardResultIndex.get + (cb.id -> (box :: rs )))
          val subCSSResult = "#progress" #> (rs.reverse ++ getBox::cs.map(_ => <span class="greybox"/>))
          resultCSS & subCSSResult
        }else{
          lastTimeAsResult<=true
          val resultCSS = c.resultSnippet(this, cb.id)
          val subCSSResult = "#progress" #> (rs.reverse ++  cs.map(_ => <span class="greybox" />))
          resultCSS & subCSSResult
        }
       }catch{
         case _:Exception => redirectTo("/CardBox/" + cb.id)
       }
      case _ => "#t" #> loc("noCardBoxSpecified")
    }
  }

  def showStatisticalData: CssSel = {
    cardbox match {
      case Full(cb) => {
        try {
          val Some(card) = lastCard.>=
          statistics.layoutOf(card.id, resolutionTime.get)
        } catch {
          case _: Exception => redirectTo("/CardBox/" + cb.id)
        }
      }
      case _ => "#t" #> loc("noCardBoxSpecified")
    }
  }

  def wrapAsHTMLSpan(textContent: String): NodeSeq = {
    <span class="caption"> {textContent} </span>
  }

  def createBoxForm:net.liftweb.util.CssSel={
    var name=""
    var description=""
    var courseId=1L

    def submitted = {
      val box = cardBoxes.create(courseId,currentUser.uid.openOrThrowException(""),name,description)
      box.save
    }


    "#courseSelector" #> SHtml.select(
      courses.findAll.map(c => new SHtml.SelectableOption(c.id+"",c.name+""))
        , Empty
        , (s) => {courseId=s.toLong}) &
    "#name"        #> SHtml.textarea("", s => { name = s}, ("rows","1"),("class","singleRow")) &
    "#description" #> SHtml.textarea( "", s => { description = s }, "style" -> "height: 450px; width: 100%;")&
    "#submit *"    #> SHtml.submit(locStr("submit"), submitted _)
  }


  import name.panitz.subato.model.mapper.CardType
  def createCardForm = {
    (cardbox) match {
      case Full(c) => {
        var answers:List[(String,Boolean)]=List()
        var question=""
        var name=""
        var explanation=""
        var cardType=CardType.MultipleChoice

        def submitted = {
        }

        val answer = <tr>
        <td>{SHtml.textarea( "", s => { explanation = s }, "style" -> "height: 1em; width: 100%;")}</td>
        <td>{SHtml.checkbox( false,s=> {})}</td>
        </tr>

        "#cardType" #> SHtml.select(
            CardType.values.map(v => new SHtml.SelectableOption(v+"",v+"")).toList
          , Full(CardType.MultipleChoice+"")
          , (s) => {cardType = CardType.withNameOpt(s).get}) &
        "#question"    #> SHtml.textarea( "", s => { question = s }, "style" -> "height: 450px; width: 100%;")&
        "#submit *"    #> SHtml.submit(locStr("submit"), submitted _) &
        "#explanation" #> SHtml.textarea( "", s => { explanation = s }, "style" -> "height: 450px; width: 100%;")&
        "#answers *"   #> <table>{List(answer,answer,answer,answer,answer,answer,answer,answer,answer,answer)}</table>
        }
      case _ =>  "*" #> <span>{loc("noCourseSpecified")}</span>
      }

  }

}
