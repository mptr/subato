package name.panitz.subato.snippet

import name.panitz.subato.model.{Card, MapperModelDependencies}
import net.liftweb.json.DefaultFormats
import net.liftweb.json.JsonAST.JValue

import scala.xml.{Node, Text}

object CourseAttributeExtractor extends  MapperModelDependencies {

  implicit val formats: DefaultFormats.type = DefaultFormats

  private val titleAttrName       = "name"
  private val descriptionAttrName = "description"
  private val privacyAttrName     = "privacy"
  private val topicsAttrName      = "topics"
  private val cardsAttrName       = "cards"
  private val courseIdAttrName    = "id"

  private[snippet] def extractCourseTitleFrom(json: JValue): String = {
    (json \ titleAttrName).extractOrElse[String]("").trim
  }

  private[snippet] def extractCourseDescriptionFrom(json: JValue): String = {
    (json \ descriptionAttrName).extractOrElse[String]("")
  }

  private[snippet] def extractCourseTopicsFrom(json: JValue): Seq[String] = {
    (json \ topicsAttrName).extractOrElse[Array[String]](Array.empty)
      .toSeq
      .filter { _.nonEmpty }
  }

  private[snippet] def extractCourseIdFrom(json: JValue): Long = {
    try {
      (json \ courseIdAttrName).extractOrElse[String]("").toLong
    } catch {
      case _: Exception => -1
    }
  }

  private[snippet] def extractCoursePrivacyFrom(json: JValue): String = {
    (json \ privacyAttrName).extractOrElse[String]("").trim
  }

  private[snippet] def isCoursePrivate(json: JValue): Boolean = {
    extractCoursePrivacyFrom(json).toLowerCase == "private"
  }

  private[snippet] def extractCardJsonsFrom(json: JValue): Seq[JValue] = (json \ cardsAttrName).children

  private[snippet] def courseCanBeCreatedFrom(json: JValue): (Boolean, Node) = {
    var errorMsgs = Seq.empty[Node]
    if (extractCourseTitleFrom(json).isEmpty) errorMsgs = <li><b>Course name</b> can not be empty</li> +: errorMsgs
    if (extractCourseDescriptionFrom(json).isEmpty) errorMsgs = <li><b>Course description</b> can not be empty</li> +: errorMsgs
    //if (extractCourseTopicsFrom(json).isEmpty) errorMsgs = <li>A course should cover at <b>least one topic</b></li> +: errorMsgs
    if (extractCardJsonsFrom(json).size < 1) errorMsgs = <li>Currently, a course should contains <b>at least 1 cards</b></li> +: errorMsgs
    if (errorMsgs.isEmpty) {
      (true, Text(""))
    } else {
      (false, <ul style='text-align: left; margin-top: 15px;'>{errorMsgs}</ul>)
    }
  }

  private[snippet] def courseCanBeUpdatedFrom(json: JValue, userId: String): Boolean = {
    val courseId = extractCourseIdFrom(json)
    if (courseId > -1) {
      flashcardCourses
        .find(courseId)
        .map(_.isOwner(userId))
        .getOrElse(false)
    } else {
      false
    }
  }

}
