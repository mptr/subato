package name.panitz.subato.comet
import net.liftweb.http.CometActor
import net.liftweb.common.Full
import scala.xml.Text
import net.liftweb.util.Schedule
import net.liftweb.http.js.JsCmds.SetHtml

import net.liftweb.common.{Box, Empty, Failure}
class Clock extends CometActor {
  override def defaultPrefix = Full("clk")   

  def render = "#time" #> timeSpan
   
  def timeSpan = (<span id="time">{net.liftweb.util.TimeHelpers.now}</span>)

  import net.liftweb.util.Helpers.TimeSpan
  // schedule a ping every 10 seconds so we redraw
  Schedule.schedule(this, Tick, TimeSpan(1000L)) 

  override def sendInitialReq_?() : Boolean = true
  override def captureInitialReq(initialReq: Box[net.liftweb.http.Req]): Unit = {
    initialReq match {
      case Full(req) =>
        println("PPPPPPPPPPPPPP"+req.path.partPath)
        //
        //duelId = req.path.partPath.last.toLong
      case Empty => println("NNNNNNNNNNNNNNNNo Request?")
      case  Failure(a, b, c) => println("FFFFFFFFFFFFFF"+a+" "+b+" "+c) 
    }
    super.captureInitialReq(initialReq)
  }


  override def lowPriority : PartialFunction[Any, Unit] = {
    case Tick => {
      println("hallo: "+net.liftweb.util.TimeHelpers.now)
      partialUpdate(SetHtml("time", Text(net.liftweb.util.TimeHelpers.now.toString)))
      // schedule an update in 10 seconds
      Schedule.schedule(this, Tick,  TimeSpan(1000L)) 
    }
  }
}
case object Tick
 
