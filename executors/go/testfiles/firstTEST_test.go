package main
import "testing"
import "./first"

func TestFac(t *testing.T) {
    got := first.Fac(5)
    if got != 120 {
        t.Errorf("first.Fac(5) = %d; want 121", got)
    }
}
func TestQuersumme(t *testing.T) {
    got := first.Quersumme(1234)
    if got != 10 {
        t.Errorf("first.Quersumme(1234) = %d; want 10", got)
    }
}
func TestFromBinary(t *testing.T) {
    got := first.FromBinary("101010")
    if got != 42 {
        t.Errorf("first.FromBinary(\"101010\") = %d; want 42", got)
    }
}
