import unittest
import factorial
import xmlrunner 

class TestFactorialMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual(factorial.factorial(5), 120)
    def test1(self):
        self.assertEqual(factorial.factorial(0), 1)
    def test2(self):
        self.assertEqual(factorial.factorial(1), 1)
    def test3(self):
        self.assertEqual(factorial.factorial(2), 2)
    def test4(self):
        self.assertEqual(factorial.factorial(3), 6)
    def test5(self):
        self.assertEqual(factorial.factorial(4), 24)
        
if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFactorialMethods)
    xmlrunner.XMLTestRunner(output=".").run(suite)

