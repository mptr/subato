module.exports = {
    
  factorial: function factorial(n) {
    if (n == 0 || n == 1)
      return 1;
    return factorial(n-1) * n;
  },
  quersumme: function quersumme(n) {
    var result = 0;
    if (n<10) return n  
      return quersumme(Math.floor(n/10)+quersumme(n%10));
  },  

  fromBinary: function fromBinary(str) {
    var result = 0;
    str.split('').forEach(letter => {
	result = result*2+(letter.charCodeAt()-'0'.charCodeAt());
    });
    return result;
  }  
}
