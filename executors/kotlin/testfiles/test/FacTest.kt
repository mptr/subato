package test
import  org.junit.Assert;

import org.junit.Test;

class FacTest {
  @Test
  fun plus1() {
    Assert.assertEquals(21, plus(17,4))
  }


  @Test
  fun fac1() {
    Assert.assertEquals(120, fac(5))
  }

  @Test
  fun ableitung1() {
    Assert.assertEquals(8.0, ableitung(4.0, {x->x*x} ),0.1)
  }

  @Test
  fun ableitung2() {
    Assert.assertEquals(8.0, ableitung({x->x*x})(4.0),0.1)
  }
    
}