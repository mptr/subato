#include "CUnit.h"
#include "adsBeispiele.h"
#include <stdio.h>

int main(int argc, char** args){
  printHeader();

  testStart("fac1");
  assertIntEq("Fakultät von 5",120,factorial(5));
  testEnd();

  testStart("fac2");
  assertIntEq("Fakultät von 0",1,factorial(0));
  testEnd();

  testStart("fac3");
  assertIntEq("Fakultät von 1",1,factorial(1));
  testEnd();

  printResults();
  printStorage();

  printFoot();
  return 0;
}